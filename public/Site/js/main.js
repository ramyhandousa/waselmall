/**
 * Created by Hesham on 15/5/2017.
 */
$(document).ready(function () {
//        nicescroll
    var nice = false;
    $(document).ready(function () {
        nice = $("html").niceScroll();
    });

//        smoothscroll
    $('.smoothscroll').on('click', function (e) {
        e.preventDefault();
        var target = this.hash
            , $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });

//        owl-carousel
    $(".owl-carousel-deals").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });

    $(".owl-carousel-items").owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });
    // brands
    var selectedClass = "";
    $(".fil-cat").click(function () {
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(100, 0.1);
        $("#portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
        setTimeout(function () {
            $("." + selectedClass).fadeIn().addClass('scale-anm');
            $("#portfolio").fadeTo(300, 1);
        }, 300);

    });

    //active state
    $(function () {
        $('.pc_select .btn').click(function () {

            $('.pc_select .btn').removeClass('active');

            var $this = $(this);
            if (!$this.hasClass('active')) {
                $this.addClass('active');
            }
            //e.preventDefault();
        });
    });

//        zoom img
    $("#zoomImg").elevateZoom({
        zoomType: "inner", // "windows", "lens"
        cursor: "-webkit-zoom-in",
        zoomWindowFadeIn: 300,
        zoomWindowFadeOut: 300,
        gallery: "gallery",
        galleryActiveClass: "active"
    });

//        date count down
    $(".getting-started").countdown("2017/05/20", function (event) {
        $(this).text(
            event.strftime('%DDays  :  %HH   %MM   %SS')
        );
    });

//        img rotation
    $('.images-rotation').imagesRotation();
    $('.images-rotation-bg').imagesRotation({
        imgSelector: 'div'
    });

//        allPic active
    $('.allPic img').click(function () {
        $(this).closest(".allPic").find("img").removeClass("active");
        $(this).addClass('active');
    });

    $(".pass").hide();
    $('.checkbox label input[type="checkbox"]').click(function () {
        $('.checkbox label input[type="checkbox"]').attr("checked", "checked");
        $(".pass").show();
    });
    $('.checkbox label input[checked="checked"]').click(function () {
        $('.checkbox label input[type="checkbox"]').removeAttr("checked", "checked");
        $(".pass").hide();
    })
});
