<!-- deal of day -->
<div class="deal text-center">
    <div class="container">
        <div class="col-xs-12">
            <!--content-->
            <div class="content">
                <h1>deal of day</h1>
                <div class="row">

                    <div class="owl-carousel owl-carousel-deals owl-theme">


                        @foreach($dealsOfDay as $deal)
                        <div class="item">
                            <a href="{{route('site.product.show')}}?id={{$deal->product->id}}">
                                <div class="images-rotation"
                                     @foreach($deal->product->images as $image)

                                     {{--data-images='["{{request()->root()  }}/public/Site/img/d1.png",--}}
                                      {{--"{{request()->root()  }}/public/Site/img/d2.png",--}}
                                       {{--"{{request()->root()  }}/public/Site/img/d3.png"]'--}}

                                     {{--data-images='["{{$image['url']}}"]'--}}
                                     {{--data-images="{{$image['url']}}"--}}

                                        @endforeach
                                >
                                    <div class="product">
                                        <div class="productImg">

                                            @if($deal->product->is_offer == 1)
                                                <div class="salary">
                                                    <p>    {{( $deal->product->offer_price /$deal->product->price   ) *100}} %  {{trans('site.index.offer')}} </p>
                                                </div>
                                            @endif

                                            <img src="{{$deal->product->images[0]['url']}}" alt="">

                                            <div class="getting-started" style="width: auto"
                                                 data-date="{{($deal->end_data != '0000-00-00') ? date('Y-m-d', strtotime($deal->end_data)) : date('Y-m-d',strtotime("+1 day"))}}">


                                            </div>

                                            <p>{{optional($deal->product->category)->name}}</p>

                                            <div class="sold">
                                                @if($deal->product->quantity == 0)
                                                        {{trans('site.index.sold_out')}}
                                                @else
                                                    {{$deal->product->quantity  }}   {{trans('site.index.quantity')}}
                                                @endif
                                            </div>

                                        </div>
                                        <h3>{{$deal->product->name}}</h3>

                                        @if($deal->product->is_offer == 1)
                                            <del>{{$deal->product->price}} </del>
                                        @else
                                            <p> <b class="badge">  {{$deal->product->currency->name}} </b> {{$deal->product->price}}   </p>
                                        @endif

                                        @if($deal->product->is_offer == 1)
                                            <span>{{$deal->product->offer_price}}</span>
                                        @endif

                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach

                        {{--<div class="item">--}}
                            {{--<a href="productDetails.html">--}}
                                {{--<div class="images-rotation"--}}
                                     {{--data-images='["img/d1.png", "img/d2.png", "img/d3.png"]'>--}}
                                    {{--<div class="product">--}}
                                        {{--<div class="productImg">--}}
                                            {{--<div class="salary">--}}
                                                {{--<p>--}}
                                                    {{--20% off--}}
                                                {{--</p>--}}
                                            {{--</div>--}}
                                            {{--<img src="{{ request()->root()  }}/public/Site/img/d2.png">--}}
                                            {{--<div class="getting-started"></div>--}}
                                            {{--<p>shirt, men</p>--}}
                                            {{--<div class="sold">--}}
                                                {{--sold out--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<h3>product name</h3>--}}
                                        {{--<del>60 sar</del>--}}
                                        {{--<span>19.40 sar</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
