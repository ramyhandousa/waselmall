@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))

@section('content')



    <div class="deal brandDetails text-center ui-content" data-role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="result">
                        <h4>{{trans('Site.search.search_research')}}</h4>
                        <div class="resultCategory content">

                            <form action="{{route('site.search')}}" method="GET">
                                <div class="form-group">
                                    <h5>{{trans('Site.search.search_text')}} <i class="fa fa-angle-right"></i> </h5>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" placeholder="{{trans('Site.search.search')}}  *">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <h5>  {{trans('Site.nav.categories')}} <i class="fa fa-angle-right"></i> </h5>
                                    <select class="form-control" name="category">
                                        <option value="" selected disabled="">{{trans('Site.search.category')}}</option>
                                        @foreach($categoriesSite as $category)
                                             <option value="{{$category->id}}"  >{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h5>  {{trans('Site.nav.brands')}} <i class="fa fa-angle-right"></i> </h5>
                                    <select class="form-control" name="brand">
                                        <option value="" selected disabled="">{{trans('Site.search.brand')}}</option>
                                        @foreach($brands as $brand)
                                            <option value="{{$brand->id}}"  >{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h5>  {{trans('Site.nav.colors')}} <i class="fa fa-angle-right"></i> </h5>
                                    <select class="form-control" name="color">
                                        <option value="" selected disabled="">{{trans('Site.search.colors')}}</option>
                                        @foreach($colors as $color)
                                            <option value="{{$color->id}}"  >{{$color->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h5>  {{trans('Site.nav.sizes')}} <i class="fa fa-angle-right"></i> </h5>
                                    <select class="form-control" name="size">
                                        <option value="" selected disabled="">{{trans('Site.search.size')}}</option>
                                        @foreach($sizes as $size)
                                            <option value="{{$size->id}}"  >{{$size->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h5>{{trans('Site.search.price')}}  <i class="fa fa-angle-right"></i> </h5>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <input type="number" min="0" oninput="validity.valid||(value='');" class="form-control" name="priceFrom" value="0" placeholder="{{trans('Site.general.form')}}">
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <input type="number" min="0" oninput="validity.valid||(value='');" class="form-control" name="priceTo" placeholder="{{trans('Site.general.to')}}">
                                        </div>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-default">{{trans('Site.search.search')}}  </button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <img style="width: 50%;margin: auto" src="{{ request()->root()  }}/public/Site/img/search.gif" class="img-responsive">
                        </div>
                        <div class="col-md-9 col-xs-12 text-left">
                            <h3>{{trans('Site.search.search_research_result')}}</h3>
                            {{--<p>visit the zara store to shop our full selection of athletic clothing and footwear from zara</p>--}}
                        </div>
                    </div>

                    @if(isset($data) && request()->query())

                        <!--content-->
                        <div class="row">
                        <div class="content">

                            @foreach($data as $value)
                                <div class="col-md-4 col-xs-12">
                                    <a href="{{route('site.product.show')}}?id={{$value->id}}">
                                        <div class="images-rotation" >
                                             {{--data-images='["img/d1.png", "img/d2.png", "img/d3.png"]'>--}}

                                            <div class="product">
                                                <div class="productImg">

                                                    @if($value->is_offer == 1)
                                                        <div class="salary">
                                                            <p>
                                                                {{ number_format( ( $value->offer_price /$value->price   ) *100 ,0)}} %
                                                            </p>
                                                        </div>
                                                    @endif

                                                    <img src="{{$value['images'][0]['url']}}" alt="">

                                                    <p>{{ optional($value->brand)->name  }}</p>
                                                    <p>{{ optional($value->category)->name  }}</p>

                                                </div>

                                                <h3>  {{$value->name}}</h3>

                                                @if($value->is_offer == 1)
                                                    <del>{{$value->offer_price}}</del>
                                                @endif

                                                <span>{{$value->price}}</span>

                                            </div>

                                        </div>
                                    </a>
                                </div>
                            @endforeach

                            <div class="col-xs-12">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous">
                                                <span aria-hidden="true">previous</span>
                                            </a>
                                        </li>
                                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li>
                                            <a href="#" aria-label="Next">
                                                <span aria-hidden="true">next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                        </div>
                    </div>

                    @endif
                </div>

            </div>
        </div>
    </div>



@endsection