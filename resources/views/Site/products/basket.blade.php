@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))

@section('content')


    @if(Auth::check())
    <!-- cart -->
    <div class="cart text-center">
        <div class="container">
            <div class="row">
                <h1>{{trans('Site.cart.cart_items')}}</h1>
                <div class="col-xs-12">
                    <form action="" method="post">
                        <div class="content cart_table">
                            <table>
                                <thead>
                                <tr>
                                    <th>{{trans('Site.cart.product_name')}}</th>
                                    <th>{{trans('Site.index.price')}}</th>
                                    <th>{{trans('Site.index.quantity')}}</th>
                                    <th>{{trans('Site.orders.value_purchases')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($cartUserSite) > 0)
                                    @foreach($cartUserSite as $cart)
                                <tr>

                                        <td>
                                            @if($cart['product_filter']->images[0])
                                                <img src="{{\App\uploadImages::whereId($cart['product_filter']->images[0])->first()->url}}" />
                                            @endif
                                            <p class="productType">
                                                {{trans("site.cart.product_name")}} :  {{$cart['product_filter']->name}}
                                            </p>
                                            <p class="productType">
                                                {{trans("site.cart.category_name")}} : {{$cart['product_filter']->category->name}}
                                            </p>
                                        </td>
                                        <td>
                                            {{$cart->price}}
                                        </td>
                                        <td>
                                            <p>  {{$cart->quantity}}  </p>
                                        </td>
                                        <td>{{$cart->price * $cart->quantity}} </td>

                                </tr>
                                    @endforeach
                                @else
                                    <div class="text-center">
                                        {{trans('Site.cart.empty')}}
                                    </div>
                                @endif
                                </tbody>
                            </table>
                            <div class="col-md-10 col-md-offset-2 col-xs-12">
                                <div class="statistics">
                                    <div class="row">

                                        <div class="col-md-8 col-xs-12">
                                            <p>
                                               {{trans('Site.orders.value_purchases')}} :
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <p  id="totalCart" data-html="{{$totalItemsPrice}}">
                                               {{$totalItemsPrice}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-xs-12">
                                            <p>
                                                {{trans('Site.orders.services_purchases')}} :
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <p id="service" data-html="{{$dataSetting['service']}}">
                                                {{$dataSetting['service']}} {{trans('Site.general.sar')}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-xs-12">
                                            <p>
                                                {{trans('Site.orders.Paiement_orders_recieving')}}
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <p id="shippp" data-html="{{$dataSetting['payment_cash']}}">
                                                {{$dataSetting['payment_cash']}} {{trans('Site.general.sar')}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-xs-12">
                                        <p>
                                           {{trans('Site.cart.total')}}
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <p id="totalPrice" >

                                        </p>
                                    </div>
                                </div>
                                <p class="payTitle">
                                  {{trans('Site.orders.value_of_order_text')}}
                                </p>
                            </div>
                            <a href="{{route('cartSite.edit',\App\Models\Cart::getIdCart(Auth::user()))}}" class="btn btn-default"> {{trans('Site.cart.edit')}}</a>
                            <a href="{{route('site.getAllOrders')}}" class="btn btn-success">{{trans('Site.orders.compete_order')}}  </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @else

     {{abort(404)}}
    @endif


@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            var  cash = parseInt('{{$dataSetting['payment_cash']}}', 10 )  ;
            var service = Number($("#service")[0].attributes[1].value);
            var shippp = Number($("#shippp")[0].attributes[1].value);
            var totalCart = Number($("#totalCart")[0].attributes[1].value);

            var  servicesAndTotalCart = totalCart + service +shippp;

            var totalPrice = $("#totalPrice");
            console.log(totalPrice)

            totalPrice.html(servicesAndTotalCart)


        });
    </script>


@endsection