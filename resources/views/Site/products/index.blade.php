@extends('Site.layouts.master')
@section('title', trans('Site.products.products'))

@section('content')


    <div class="deal brandDetails text-center">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-xs-12">
                    <div class="result">
                        <h4>{{trans('site.products.show_result')}}</h4>
                        <div class="resultCategory">
                            <h5>
                                @if(isset($category))
                                    {{$category->name}}
                                @endif
                                    <i class="fa fa-angle-right" ></i>
                            </h5>

                            @if(isset($brandsProducts))

                                @foreach( $brandsProducts as $brand)

                                     <a href="{{route('site.getAllProducts')}}?id={{$category->id}}&&brandId={{$brand->id}}">{{$brand->name}}</a>
                                    <br><br>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-xs-12">
                    <div class="row">
                        @if(isset($category))
                            <div class="col-md-3 col-xs-12">
                                {{--// photo of Brand--}}
                                @if(request('brandId') && $brandsProducts->where('id',request('brandId'))->first()->image)
                                    <img src="{{$brandsProducts->where('id',request('brandId'))->first()->image}}" class="img-responsive">
                                @else
                                    <img src="{{$category->image}}" class="img-responsive">
                                @endif
                            </div>
                            <div class="col-md-9 col-xs-12 text-left">

                                <h3>{{trans('site.products.store_static',['attribute' => $category->name ])}} </h3>
                                <p> {{trans('site.products.text_static',['attribute' => $category->name ])}}</p>

                            </div>
                        @endif
                    </div>
                    <!--content-->
                    <div class="row">
                        <div class="content">
                            @foreach($products  as $product )
                            <div class="col-md-4 col-xs-12">
                                <a href="{{route('site.product.show')}}?id={{$product->id}}">
        {{--<div class="images-rotation"  data-images='["{{request()->root()  }}/public/Site/img/d1.png", "{{request()->root()  }}/public/Site/img/d2.png",--}}
                                          {{--"{{request()->root()  }}/public/Site/img/d3.png"]'>--}}


                                        <?php
                                        $data = [];

                                        foreach ($product["images"] as $image){
                                            $data[] = $image['url'];
                                        }


                                        ?>

                                        <div class="images-rotation"  data-images='<?=  json_encode($data,JSON_UNESCAPED_SLASHES); ?> '>

                                        <div class="product">
                                            <div class="productImg">

                                                @if($product->is_offer == 1)
                                                    <div class="salary">
                                                        <p>   {{ number_format( ( $product->offer_price /$product->price   ) *100 ,0)}} %  {{trans('site.index.offer')}} </p>
                                                    </div>
                                                @endif

                                                    <img src="{{$product->images[0]['url']}}" alt="">


                                                {{--@if($product->offer_day != null)--}}
                                                        {{--<div  style="width: auto" class="getting-started"--}}
                                                          {{--data-date="{{(optional($product->offer_day)->end_data != '0000-00-00') ? date('Y-m-d', strtotime(optional($product->offer_day)->end_data)) : date('Y-m-d',strtotime("+1 day"))}}"--}}
                                                        {{--></div>--}}
                                                {{--@endif--}}

                                                @if(isset($category))
                                                        <p>{{$category->name}} </p>
                                                @endif

                                            </div>
                                            <h3>{{$product->name}}</h3>
                                            @if($product->is_offer == 1)
                                                <del>{{$product->price}}  </del>
                                            @else
                                                <p>  {{$product->price}}   {{$product->currency->name}}</p>

                                            @endif
                                            @if($product->is_offer == 1)
                                                <span> {{$product->offer_price}} {{$product->currency->name}} </span>
                                            @endif
                                        </div>
                                    </div>
                                </a>
                            </div>

                            @endforeach
                            <div class="col-xs-12">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous">
                                                <span aria-hidden="true">previous</span>
                                            </a>
                                        </li>
                                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li>
                                            <a href="#" aria-label="Next">
                                                <span aria-hidden="true">next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>




@endsection