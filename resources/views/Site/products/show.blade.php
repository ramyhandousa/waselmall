@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))

@section('content')
    <!-- product details -->
    <div class="productDetails">
        <div class="container">
            <div class="col-md-5 col-xs-12">
                <div class="detail-caption">
                    <img id="zoomImg" src="{{$product['images'][0]['url']}}" data-zoom-image="{{$product['images'][0]['url']}}"/>
                </div>

                <div id="gallery" class="row allPic">

                    @foreach( $product['images'] as $image)
                        <div class="col-md-3 col-xs-6">
                            <a href="#" class="elevatezoom-gallery active" data-image="{{$image['url']}}"
                               data-zoom-image="{{$image['url']}}">
                                <img src="{{$image['url']}}" class="img-responsive  "></a>
                        </div>
                    @endforeach

                </div>


            </div>
            <div class="col-md-7 col-xs-12">
                <h1>{{$product->name}}
                    @if(isset($product_purchases) && count($product_purchases) > 0)
                        @foreach($product_purchases as $purchase)
                    <b class="badge">{{$purchase->name}}</b>
                        @endforeach
                    @endif
                </h1>
                <h4>{{optional($product->category)->name}}</h4>
                <h2>
                    @if($product->is_offer == 1)
                     <span>{{ $product->price - $product->offer_price}} {{ optional($product->currency)->name}} </span>
                    @else
                        <span>{{$product->price}} {{ optional($product->currency)->name}} </span>
                    @endif

                    @if($product->is_offer == 1)
                        <del>{{$product->price}} </del>
                        <b> {{ number_format( ( $product->offer_price /$product->price   ) *100 ,0)}} % </b>
                    @endif

                </h2>
                <p><b>{{trans('site.products.availability')}} : </b> @if($product->quantity != 0) {{trans('site.products.in_stock')}}@else{{trans('site.products.not_in_stock')}} @endif</p>
                <p><b>{{trans('site.products.product_code')}} : </b>{{$product->id}}</p>
                <p><b>{{trans('site.products.review_points')}} : </b>20 points</p>
                @if($product->offer_day != null)
                    <p><b>{{trans('site.products.offers')}} : </b>{{trans('site.products.text_static_offer',['attribute' => $product->name])}}</p>
                @endif
                <p><b>{{$product->description}}</b></p>

                <form method="post" action="{{route('cartSite.store')}}" data-parsley-validate  novalidate>
                    {{ csrf_field() }}
                    <input type="hidden" name="productId" value="{{$product->id}}">
                    @if($product->is_offer == 1)
                        <input type="hidden" name="price" value="{{$product->offer_price}}">
                    @else
                        <input type="hidden" name="price" value="{{$product->price}}">
                    @endif

                    <div class="row">
                        <div class="col-md-9 col-xs-12 row">
                            <div class="col-md-8 col-xs-12">
                                <select class="form-control" name="productSize"  required>
                                    <option value="" disabled selected>{{trans('site.products.select_size')}}</option>
                                    @if(isset($productsSizes) && count($productsSizes) > 0)
                                        @foreach($productsSizes as $size)
                                        <option value="{{$size['id']}}">{{$size['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-xs-12 row">
                            <div class="col-md-4 col-xs-12">
                                @if(isset($productsColors) && count($productsColors) > 0)
                                    @foreach($productsColors as $color)
                                        <div class="radio" >
                                            <label style="font-size: 1.5em">
                                                <input type="radio" name="productColor" value="{{$color['id']}}" checked="">
                                                <span class="cr" style="background-color: {{ '#'.$color['hash_code']}}"><i class="cr-icon fa fa-circle"></i></span>

                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-md-5 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">{{trans('site.products.quantity')}}</span>
                                    <input type="number" class="form-control"  required min="1"
                                           max="{{$product->quantity}}" data-parsley-required-message="{{trans('site.products.required',['attribute' => 'الكمية'])}}"
                                           data-parsley-max-message="{{trans('site.products.max_quantity',['attribute' => $product->quantity])}}"
                                           placeholder="0"
                                           name="quantity" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <a href="javascript:;" class="btn btn-default"   onclick="favourite( {{$product->id }} )">
                                    <i  class="fa fa-heart" @if($product->is_favorite == 1) style="color: red" @endif></i>
                                </a>
                                <button  class="btn btn-default"  ><i class="fa fa-share"></i></button>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-warning">{{trans('site.products.add_cart')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(count($dealsOfDay) > 0)

        @include('Site.deals_of_day')

    @endif


@endsection

@section('scripts')


    <script type="text/javascript">



        function favourite($productId) {

            var productId = $productId;

            $.ajax({
                type: 'POST',
                url: '{{ route('site.user.makeFavourite') }}',
                data: {
                    productId: productId
                },
                dataType: 'json',
                cache: false,
                success: function (data) {

                    if (data.status == 200 || data.status == 201) {

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = "@lang('Site.general.success')";
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                        setTimeout(function () {
                            window.location.reload()
                        },1000)

                    }

                    if (data.status == 400) {
                        var shortCutFunction = 'error';
                        var msg = data.error[0];
                        var title = "@lang('Site.general.error')";
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }
                }
            });

        }


    </script>

@endsection