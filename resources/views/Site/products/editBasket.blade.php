
@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))

@section('styles')
    <style>

        .loaderCart{
            display: none !important;
        }


    </style>
@endsection
@section('content')


<!-- cart -->
<div class="cart text-center">
    <div class="container">
        <div class="row">
            <h1> {{trans('Site.cart.edit_items')}} </h1>
            <div class="loaderCart">
                <img src="{{ request()->root()  }}/public/Site/img/editCart.gif">
            </div>
            <div class="col-xs-12" id="myTable">
                <form id="submitForm"   method="post" action="{{ route('cartSite.update', $cart->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}



                    <div class="content cart_table">
                        <table id="items-cart">
                            <thead>
                            <tr>
                                <th>{{trans('Site.cart.product_name')}}</th>
                                <th>{{trans('Site.index.price')}}</th>
                                <th>{{trans('Site.index.quantity')}}</th>
                                <th>{{trans('Site.orders.value_purchases')}}</th>
                                <th>{{trans('Site.cart.delete')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($cartUserSite) > 0)
                                <input type="hidden" value="{{ count($cartUserSite) }}" name="cartCount" />

                                @foreach($cartUserSite as $cart)
                                    <input type="hidden" name="cartIds[]" value="{{ $cart->id }}"/>
                            <tr class="item-card">
                                    <td>
                                        @if($cart['product_filter']->images[0])
                                            <img src="{{\App\uploadImages::whereId($cart['product_filter']->images[0])->first()->url}}"  />
                                        @endif
                                        <p>
                                            {{trans('Site.products.select_size')}}
                                        </p>
                                        <div class="form-group">
                                            <select class="form-control" id="size{{$cart->id}}" name="sizes[]">
                                                @foreach($sizes as $size)
                                                    <option value="{{$size->id}}" @if(optional($cart['options'])['size_id'] == $size->id) selected @endif>
                                                        {{$size->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <p>
                                            {{trans('Site.products.select_color')}}
                                        </p>
                                        <div class="form-group">
                                            <select class="form-control" id="color{{$cart->id}}"  name="colors[]">
                                                @foreach($colors as $color)
                                                    <option value="{{$color->id}}" @if(optional($cart['options'])['color_id'] == $color->id) selected @endif>
                                                        {{$color->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td class="myPrice{{$cart->id}}">   {{$cart->price}}  </td>

                                    <td>
                                        <div class="form-group  " data-item=" {{$cart->price}}">
                                            <input type="number" id="myQuantity{{$cart->id}}" min="1" data-id="{{$cart->id}}"  class="form-control quantity btn-number"
                                                   value="{{$cart->quantity}}"
                                                   name="quantity[]"
                                                   placeholder="quantity *">
                                        </div>
                                    </td>

                                    <td class="count-item-price">

                                            {{$cart->price * $cart->quantity}}

                                    </td>

                                    <td>
                                        <input  value="{{$cart->product_id}}"  style="display: none" id="productId{{$cart->id}}">

                                            {{--<a href="javascript:;" id="editCart" --}}
                                               {{--data-id="{{ $cart->id }}"--}}
                                                    {{--data-toggle="tooltip" data-placement="top"--}}
                                                    {{--data-original-title=" تعديل" >--}}
                                                {{--<img width="10%"  src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">--}}
                                            {{--</a>--}}

                                        {{--<a  href="#"--}}
                                           {{--data-id="{{ $cart->id }}"--}}
                                           {{--data-url="{{ route('cartSite.update', $cart->id) }}"--}}
                                           {{--class="editBasket btn btn-default">{{trans('Site.cart.edit')}}--}}
                                        {{--</a>--}}




                                        <a href="javascript:;" data-url="{{ route('site.cart.delete') }}" id="elementRow{{ $cart->id }}" data-id="{{ $cart->id }}"
                                           class="removeElement btn btn-icon ">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>


                            </tr>
                                @endforeach
                            @else
                                <div class="text-center">
                                    {{trans('Site.cart.empty')}}
                                </div>
                            @endif
                            </tbody>
                        </table>
                        <div class="col-md-10 col-md-offset-2 col-xs-12">
                            {{--<div class="statistics">--}}
                                {{--<div class="row">--}}

                                    {{--<div class="col-md-8 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--{{trans('Site.orders.value_purchases')}} :--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--34.00 sar--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-8 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--{{trans('Site.orders.services_purchases')}} :--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--10.00 sar--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-8 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--{{trans('Site.orders.Paiement_orders_recieving')}}--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 col-xs-12">--}}
                                        {{--<p>--}}
                                            {{--10.00 sar--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <p>
                                        {{trans('Site.cart.total')}}
                                    </p>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <p id="totalPriceOfCart">
                                      {{$totalItemsPrice}}
                                    </p>
                                </div>
                            </div>
                            <p class="payTitle">
                                {{trans('Site.orders.value_of_order_text')}}
                            </p>
                        </div>
                        <button type="submit" class="btn btn-default">{{trans('Site.cart.edit')}}</button>
                    </div>
                </form>



            </div>
        </div>
    </div>
</div>



@endsection

@section('scripts')

<script>
    $(document).ready(function () {

        $('#items-cart').on('keyup', '.btn-number', function () {

            const count = $(this).parent().parent().children().children()[0].value;
            const itemPrice = $(this).parent().data('item');

            $(this).closest('.item-card').find('.count-item-price').html(count * itemPrice);

            const itemsPrices = document.getElementsByClassName('count-item-price');

            var totalItemsPrices = 0;
            Array.prototype.forEach.call(itemsPrices, child => {
                totalItemsPrices += parseInt(child.textContent, 10);
            });

            $('#totalPriceOfCart').html(totalItemsPrices);

        });

    });




//    $("#editCart").on('click', function (myButton) {
//        $(".loaderCart").attr('style','display: block !important');
//        $("#myTable").attr('style','display: none !important');
//        setTimeout(function () {
//            $(".loaderCart").attr('style','display: none !important');
//            $("#myTable").attr('style','display: block !important');
//        },2000)
//    });

$(".editBasket").click(function () {
    var id = $(this).attr('data-id');
    var url = $(this).attr('data-url');
    var productId = $("#productId"+ id).val();
    var size = $("#size"+ id).val();
    var color = $("#color"+ id).val();
    var quantity = $("#myQuantity"+ id).val();

    $.ajax({
        type: 'PUT',
        url: url ,
        data: {
            'cart_id' : id,
            'product_id' : productId,
            'size' : size,
            'color' : color,
            'quantity' : quantity,
        },
        dataType: 'json',
        cache: false,
        success: function (data) {

            if (data.status == 200 ) {

                var shortCutFunction = 'success';
                var msg = data.message;
                var title = "@lang('Site.general.success')";
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                setTimeout(function () {
                    window.location.reload()
                },1000)

            }

            if (data.status == 400) {
                var shortCutFunction = 'error';
                var msg = data.message;
                var title = "@lang('Site.general.error')";
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                setTimeout(function () {
                    window.location.reload()
                },1000)

            }
        }
    });
});



</script>

    <script>

        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            swal({
                title: "@lang('Site.general.sure')",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('Site.general.yes')",
                cancelButtonText: "@lang('Site.general.close')",
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {

                                var shortCutFunction = 'success';
                                var msg = "@lang('Site.cart.delete_success')";
                                var title = "@lang('Site.general.success')";
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                                $tr.find('td').fadeOut(1000, function () {
                                    $tr.remove();
                                });
                                setTimeout(function () {
                                    window.location.reload()
                                },1000)
                            }

                        }
                    });
                }
            });
        });





        $(document).ready(function () {
            //$('#datatable').dataTable();
            //$('#datatable-keytable').DataTable( { keys: true } );
//            $('#datatable-responsive').DataTable();

        });


    </script>


@endsection
