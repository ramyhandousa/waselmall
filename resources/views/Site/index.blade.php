@extends('Site.layouts.master')
@section('title', trans('Site.general.home'))

@section('content')

    <div class="mainCategory text-center">
        <div class="container">
            <div class="col-xs-12">
                <!--content-->
                <div class="content">

                    <!-- third category -->

                    @foreach($categoriesSite as $category)

                        <div class="col-md-4 col-xs-12">
                            <a href="{{route('site.getAllProducts')}}?id={{$category->id}}">
                                <div class="imgContent">
                                    <div class="txt3">
                                        <h4>{{$category->name}}</h4>
                                        {{--<h1>women</h1>--}}
                                    </div>
                                    <img src="{{$category->image}}" class="img-responsive">
                                </div>
                            </a>
                        </div>

                    @endforeach

                    <div class="clearfix"></div>

                    <div class="offers">
                        <!-- first offer -->
                        @foreach($purchase_types as $purchase_type)
                            <div class="col-md-4 col-xs-12">
                                <a href="#">
                                    <h3>{{$purchase_type->name}}</h3>
                                    <p> {{trans('site.index.value')}}  {{$purchase_type->price}} </p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- deal of day -->
    @if(count($dealsOfDay) > 0)

    @include('Site.deals_of_day')

    @endif

    <!--  End deal of day -->


    <!-- brands of day -->

    @include('Site.brands')
    <!-- End brands of day -->


@endsection



