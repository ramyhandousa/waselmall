<h1>{{trans('Site.profile.personal_data')}}  </h1>
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href="#">select</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                <li  ><a href="{{route('usersSite.show',Auth::id())}}">{{trans('site.profile.account_dashboard')}}    </a></li>
                <li  ><a href="{{route('usersSite.edit',Auth::id())}}">{{trans('site.profile.account_information')}}   </a></li>
                <li><a href="{{route('site.editAddress')}}">{{trans('site.profile.account_book')}}  </a></li>
                <li><a href="#">{{trans('site.profile.account_orders')}}  </a></li>
                <li><a href="{{route('site.listFavourite')}}">{{trans('site.profile.account_wishlist')}} </a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>