@extends('Site.layouts.master')
@section('title', trans('Site.profile.personal_data'))
@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')


    <!-- account information -->
    <div class="account information">
        <div class="container">
            <div class="row">

                @include('Site.user.nav')

                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <div class="content">
                        <h1> {{trans('Site.profile.add_account_book')}}    </h1>
                        <form method="post" action="{{route('site.user.addAddress')}}">
                            {{ csrf_field() }}
                            <div class="col-md-6 col-xs-12">
                                <p> {{trans('Site.profile.account_book')}}</p>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="company" placeholder="{{trans('Site.register_details.company')}}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telephone" placeholder="{{trans('Site.register_details.telephone')}}  *">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="fax" placeholder="{{trans('Site.register_details.fax')}} *">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                            <p>  {{trans('Site.profile.account_book')}}</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" placeholder="{{trans('Site.register_details.address')}}  *">
                                </div>
                                <div class="form-group">
                                    <select class="form-control city" required name="city">
                                        <option value="" disabled selected>{{trans('Site.register_details.city')}}  </option>
                                        @foreach( $countery  as $city)
                                            <option value="{{ $city->id }}"  >{{$city->name}}  </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" id="myCountery">
                                    <select class="form-control City_chid" name="city">
                                        <option value="" disabled selected>{{trans('Site.register_details.region')}}  </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 btns">
                                <a href="#" onclick="window.history.back();return false;" class="btn btn-success">{{trans('Site.general.back')}}</a>
                                <button   id="hiddenButton" class="btn btn-default">  {{trans('Site.profile.add_account_book')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script>

        $(document).ready(function() {

            $(document).on('change', '.city', function () {

                var id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');

                var myButton = document.getElementById("hiddenButton");
                console.log(myButton);
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': id},
                    success:function (data) {

                        if (data == 401){
                            myButton.classList.remove("hidden-category");
                            $("#myCountery").hide();

                        }else {
                            $("#myCountery").show();
                            myButton.classList.add("hidden-category");
                            op += '<option  disabled selected>إختر المدينة</option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }


                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

            $(document).on('click', '.City_chid', function () {

                var Child_City_chid =   $(this).val();
                var myButton = document.getElementById("hiddenButton");
                var myCountery = document.getElementById('myCountery');
                if (Child_City_chid != null || Child_City_chid != undefined ){

//                     $("#myCountery").hide();
                    myButton.classList.remove("hidden-category");
                }

            });

        });


    </script>


@endsection