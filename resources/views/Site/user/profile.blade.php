@extends('Site.layouts.master')
@section('title', trans('Site.profile.personal_data'))

@section('content')


    <!-- account -->
    <div class="account">
        <div class="container">
            <div class="row">

                @include('Site.user.nav')

                <div class="col-xs-12">
                    <div class="col-md-4 col-xs-12">
                        <div class="content">
                            <h1><img src="{{$user->image}}"> {{trans('site.profile.welcome')}}</h1>
                            <h4>{{$user->fullname}}</h4>
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>--}}
                            {{--<h3>your coupon codes</h3>--}}
                            {{--<p>use "<b>grap</b>" for 15% off everything</p>--}}
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="content">
                            <h1 class="title">{{trans('site.profile.account_information')}} </h1>

                            <h5>{{trans('site.profile.name')}}</h5>
                            <p>{{$user->fullname}}</p>

                            <h5>{{trans('site.register_details.phone')}}</h5>
                            <p>{{$user->phone}}</p>

                            <h5>{{trans('site.register_details.email')}} </h5>
                            <p>{{$user->email}} </p>

                            <h5>{{trans('site.profile.date_of_birth')}} </h5>
                            <p>{{$user->data_of_birth}} </p>

                            <h5>{{trans('site.register_details.gender')}} </h5>
                            <p>  @if($user->gender == 1){{trans('site.register_details.gender_male')}} @else {{trans('site.register_details.gender_female')}} @endif</p>

                            <a href="{{route('usersSite.edit',$user->id)}}" class="btn btn-default">{{trans('site.profile.update_profile')}}</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="content">
                            <h1 class="title">  {{trans('site.profile.account_book')}}</h1>

                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h5>  {{trans('site.profile.address')}}      </h5>
                                    <p>{{$user->address}}</p>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <h5>{{trans('site.register_details.region')}}      </h5>
                                    <p>{{optional( $user->city)->name}}</p>
                                </div>


                                @if( optional($user->city)->parent )
                                    <div class="col-md-6 col-xs-12">
                                        <h5>{{trans('site.register_details.city')}}      </h5>
                                        <p> {{ optional($user->city->parent)->name}}</p>
                                    </div>

                                @endif

                                @if( $user->company_name )
                                    <div class="col-md-6 col-xs-12">
                                        <h5>{{trans('site.register_details.company')}}      </h5>
                                        <p> {{ $user->company_name}}</p>
                                    </div>

                                @endif

                                @if( $user->company_phone )
                                    <div class="col-md-6 col-xs-12">
                                        <h5>{{trans('site.register_details.company_phone')}}      </h5>
                                        <p> {{ $user->company_phone  }}</p>
                                    </div>

                                @endif

                                @if( $user->company_fax )
                                    <div class="col-md-6 col-xs-12">
                                        <h5>{{trans('site.register_details.fax')}}      </h5>
                                        <p> {{ $user->company_fax }}</p>
                                    </div>

                                @endif

                            </div>

                            {{--<a href="book.html" class="btn btn-default">update</a>--}}
                            <a href="{{route('site.editAddress')}}" class="btn btn-default">{{trans('Site.profile.add_account_book')}}</a>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection