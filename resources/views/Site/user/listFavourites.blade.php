
@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))

@section('content')



<!-- account information -->
<div class="account information wishlist">
    <div class="container">
        <div class="row">

            @include('Site.user.nav')

            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="content">
                    <h1> {{trans('Site.profile.account_wishlist')}}</h1>
{{--                    <a href="#" class="btn btn-default">{{trans('Site.cart.all_in_cart')}}</a>--}}
                    <div class="clearfix"></div>   <br>
                    <div class="row">
                        @foreach($products  as $product )
                        <div class="col-md-3 col-xs-12">
                            <a href="#"><img src="{{$product->images[0]['url']}}" class="img-responsive" /></a>

                            <a href="#"> <h5>{{$product->name}}</h5></a>
                            <h5>
                                <b>{{trans('Site.products.size')}} : </b><br>
                            @foreach($product->size as $size)
                                    <b class="badge" style="margin-top: 5px"> {{$size['name']}}</b>
                                <br>

                            @endforeach
                            </h5>
                            <h5>   {{$product->price}}  {{ optional($product->currency)->name }}</h5>
                            <a   href="javascript:;"  onclick="addBasket( {{$product->id }} , {{$product->quantity}} , {{$product->price}} )"  class="btn btn-default">{{trans('Site.cart.add_to')}}</a>
                            <a  href="javascript:;"  onclick="removeFavourite( {{$product->id }}  )" class="btn btn-success">{{trans('Site.cart.remove')}}</a>
                        </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')


    <script type="text/javascript">

        function addBasket($productId ,$quantity , $price) {

            var productId = $productId;
            var quantity = $quantity;
            var price = $price;

            $.ajax({
                type: 'POST',
                url: '{{ route('cartSite.store') }}',
                data: {
                    productId: productId,
                    quantity: quantity,
                    price: price,
                },
                dataType: 'json' ,
                cache: false,
                success: function (data) {

                    if (data.status == 200) {

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }

                    if (data.status == 400) {
                        var shortCutFunction = 'error';
                        var msg = data.error[0];
                        var title = 'خطأ';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }
                },
                complete: function () {
                    window.location.reload()
                }
            });

        }
        function removeFavourite($productId  ) {

            var productId = $productId;

            $.ajax({
                type: 'POST',
                url: '{{ route('site.user.makeFavourite') }}',
                data: {
                    productId: productId,
                },
                dataType: 'json' ,
                cache: false,
                success: function (data) {

                    if (data.status == 201) {

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }

                    if (data.status == 400) {
                        var shortCutFunction = 'error';
                        var msg = data.error[0];
                        var title = 'خطأ';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    }
                },
                complete: function () {

                    setTimeout(function () {
                        window.location.reload()
                    },1000)

                }
            });

        }


    </script>

@endsection
