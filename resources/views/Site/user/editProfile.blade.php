@extends('Site.layouts.master')
@section('title', trans('Site.profile.personal_data'))

@section('content')


<!-- account information -->
<div class="account information">
    <div class="container">
        <div class="row">

            @include('Site.user.nav')

            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="content">
                    <h1> {{trans('Site.profile.edit_personal_data')}}</h1>
                    <form method="POST" action="{{ route('usersSite.update', $user->id) }}" >
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="col-md-6 col-xs-12">
                            <p>   {{trans('Site.profile.personal_data')}}   </p>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$user->first_name}}" name="first_name"  placeholder="{{trans('Site.register_details.first_name')}} *">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$user->last_name}}" name="last_name" placeholder="  {{trans('Site.register_details.last_name')}} *">
                            </div>
                            <div class="form-group ">
                                <input type="email" class="form-control" value="{{$user->email}}" name="email" placeholder="{{trans('Site.register_details.email')}}">
{{--                                @if($errors->has('email'))--}}
                                    {{--<p class="help-block">{{ $errors->first('email') }}</p>--}}
                                {{--@endif--}}
                            </div>
                            <div class="form-group ">
                                <input type="number" class="form-control" value="{{$user->phone}}" name="phone" placeholder="{{trans('Site.register_details.phone')}}">
{{--                                @if($errors->has('phone'))--}}
{{--                                    <p class="help-block">{{ $errors->first('phone') }}</p>--}}
                                {{--@endif--}}
                            </div>
                            <div class="checkbox" >
                                <label>
                                    <input type="checkbox" id="checkPassword">
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                   {{trans('Site.profile.change_password')}}
                                </label>
                            </div>
                            <div class="form-group   showPassword">
                                <input type="password" class="form-control" name="currentPass" placeholder="  {{trans('Site.profile.current_password')}} *">
                            </div>
                            <div class="form-group   showPassword">
                                <input type="password" class="form-control" id="password1" name="newPass" placeholder="  {{trans('Site.profile.new_password')}} *">
                            </div>
                            <div class="form-group   showPassword">
                                <input type="password" class="form-control"
                                       id="confirm_password1"
                                       data-parsley-equalto="#pass1"
                                       data-parsley-equalto-message ='غير مطابقة لكلمة المرور'
                                       name="confirmPass"
                                       placeholder=" {{trans('Site.register_details.confirm_password')}} *">
                            </div>

                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p>{{trans('Site.profile.date_of_birth')}}</p>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <select class="form-control" name="day">
                                            <option value="" disabled selected>{{trans('Site.register_details.day')}}</option>
                                            @for ($i = 1; $i <= 12; $i++)
                                                <option value="{{ $i }}"  >{{$i}}  </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <select class="form-control" name="month">
                                            <option value="" disabled selected>{{trans('Site.register_details.month')}} </option>
                                            @for ($i = 1; $i <= 12; $i++)
                                                <option value="{{ $i }}"  >{{$i}}  </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <select class="form-control" name="year">
                                            <option value="" disabled selected>{{trans('Site.register_details.year')}}  </option>
                                            @foreach( array_reverse(range( date('Y'), 1990 ) ) as $i)
                                                <option value="{{ $i }}"  >{{$i}}  </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="gender">
                                    <option value="" disabled selected>{{trans('Site.register_details.gender')}}</option>
                                    <option value="1" > {{trans('Site.register_details.gender_male')}} </option>
                                    <option value="2"> {{trans('Site.register_details.gender_female')}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 btns">
                            <a href="#"  onclick="window.history.back();return false;" class="btn btn-success">{{trans('Site.general.back')}}</a>
                            <button   class="btn btn-default">{{trans('Site.profile.update')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

    <script>

        $(document).ready(function() {

            var inputPassword = $(".showPassword");
            var inputChecked =  $("#checkPassword");

            $(inputPassword).hide();

            $(inputChecked).on("click", function () {

                if (inputChecked[0].checked  ){

                    $(inputPassword).toggle();

                }else {

                    $(inputPassword).toggle();
                }


            });


            var password = document.getElementById("password1")
                , confirm_password = document.getElementById("confirm_password1");

            function validatePassword(){
                if(password.value != confirm_password.value) {
                    confirm_password.setCustomValidity("Passwords Don't Match");
                } else {
                    confirm_password.setCustomValidity('');
                }
            }

            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;


        });

    </script>


@endsection