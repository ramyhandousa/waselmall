@extends('Site.layouts.master')
@section('title', trans('Site.products.details_products'))
@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')

    <!-- cart -->
    <div class="cart text-center">
        <div class="container">
            <form method="POST" action="{{route('site.storeOrder')}}">
                {{ csrf_field() }}
            <div class="row">

                <h1>{{trans('Site.orders.compete_order')}}</h1>
                <div class="col-md-4 col-xs-12">
                    <h4><span>1</span> {{trans('Site.profile.account_information')}}  </h4>
                    <div class="content">

                            <div class="form-group">
                                <input type="text" class="form-control"   value="{{$user->first_name}}" placeholder="{{trans('Site.register_details.first_name')}}   *">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  value="{{$user->last_name}}" placeholder="{{trans('Site.register_details.last_name')}}  *">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control"   value="{{$user->address}}" placeholder="{{trans('Site.register_details.address')}}   *">
                            </div>

                            @if( optional($user->city)->parent )


                                <div class="form-group changeCurrentAddress" >
                                    <input type="text" class="form-control"
                                           value="{{optional($user->city->parent)->name}}"

                                           readonly
                                           placeholder="{{trans('Site.register_details.city')}}   *">
                                </div>

                                {{--<select class="form-control" name="city">--}}
                                {{--<option value="{{ optional($user->city->parent)->id}}">{{ optional($user->city->parent)->name}}</option>--}}
                                {{--</select>--}}

                            @endif

                            <div class="form-group changeCurrentAddress ">


                                <div class="form-group">
                                    <input type="text" class="form-control"
                                           value="{{optional($user->city)->name}}"
                                           readonly
                                           placeholder="{{trans('Site.register_details.region')}}   *">
                                </div>

                                {{--<select class="form-control" name="city">--}}
                                    {{--<option value="{{optional($user->city)->id}}">{{optional($user->city)->name}}</option>--}}
                                {{--</select>--}}

                            </div>


                        <div class="form-group changeCurrentAddress" style="display: none">

                            <select class="form-control city"  id="cityChoose" name="cityChange">
                                <option value="" disabled selected>{{trans('Site.register_details.city')}}  </option>
                                @foreach( $countery  as $city)
                                    <option value="{{ $city->id }}"  >{{$city->name}}  </option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group changeCurrentAddress   " id="myCountery" style="display: none">

                            <select class="form-control City_chid" name="cityChange">
                                <option value="" disabled selected>{{trans('Site.register_details.region')}}  </option>
                            </select>
                        </div>



                            <p class="txt">{{trans('Site.general.enter_phone_english')}} </p>
                            <p>{{trans('Site.register_details.phone')}}   *</p>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="country_code">{{trans('Site.general.code_city')}}    </label>
                                        <input type="number" class="form-control"
                                               value="@if($user->phone){{substr($user->phone, 0, 2) }}@endif"

                                               id="country_code"/>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="form-group">
                                        <label for="number">{{trans('Site.login.phone')}}  </label>
                                        <input type="number" class="form-control"
                                               value="@if($user->phone){{ ltrim($user->phone,"05")}}@endif"
                                                id="number"/>
                                    </div>
                                </div>
                            </div>
                            <p> {{trans('Site.orders.another_phone')}}    </p>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="country_code1">{{trans('Site.general.code_city')}}  </label>
                                        <input type="number" class="form-control" name="country_code" id="country_code1"/>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="form-group">
                                        <label for="number1">{{trans('Site.login.phone')}}  </label>
                                        <input type="number" class="form-control" name="phone" id="number1"/>
                                    </div>
                                </div>
                            </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="addressCheck" id="checkedAddress"  >
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                {{trans('Site.orders.another_location')}}
                             </label>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <h4><span>2</span> {{trans('Site.orders.review_order')}}    </h4>
                    <div class="content cart_table">
                        <table>
                            <thead>
                            <tr>
                                <th>{{trans('Site.cart.product_name')}}  </th>
                                <th>{{trans('Site.index.price')}}  </th>
                                <th>{{trans('Site.index.quantity')}}  </th>
                                <th>{{trans('Site.orders.value_purchases')}}  </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cartUserSite as $cart)
                            <tr>

                                <td>
                                    @if($cart['product_filter']->images[0])
                                        <img src="{{\App\uploadImages::whereId($cart['product_filter']->images[0])->first()->url}}" class="img-responsive"/>
                                    @endif
                                    <p>
                                        {{$cart['product_filter']->name}}
                                    </p>

                                </td>
                                <td>  {{$cart->price}}</td>
                                <td>{{$cart->quantity}}</td>
                                <td>{{$cart->price * $cart->quantity}} </td>

                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-10 col-md-offset-2 col-xs-12">
                            <div class="statistics">
                                <div class="row">

                                    <div class="col-md-8 col-xs-12">
                                        <p>
                                            {{trans('Site.orders.value_purchases')}}   :
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <p id="totalCart" data-html="{{$totalItemsPrice}}">
                                            {{$totalItemsPrice}}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-xs-12">
                                        <p>
                                            {{trans('Site.orders.services_purchases')}}
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <p id="service" data-html="{{$dataSetting['service']}}" >
                                            {{$dataSetting['service']}} {{trans('Site.general.sar')}}
                                        </p>
                                    </div>
                                </div>
                                <div class="row" id="rowHidden"  >
                                    <div class="col-md-8 col-xs-12">
                                        <p>
                                            {{trans('Site.orders.Paiement_orders_recieving')}}
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <p  id="shippp">
                                            {{$dataSetting['payment_cash']}} {{trans('Site.general.sar')}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <p>
                                        {{trans('Site.cart.total')}}    :
                                    </p>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <p id="totalPrice">

                                    </p>
                                    <input type="hidden" id="totalHidden" value=" " name="totalPrice">
                                </div>
                            </div>
                            <p class="payTitle">
                                {{trans('Site.orders.value_of_order_text')}}
                            </p>
                        </div>
                        <a href="{{route('cartSite.edit',\App\Models\Cart::getIdCart(Auth::user()))}}"
                           class="btn btn-default">
                            {{trans('Site.cart.edit')}}
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <h4><span>4</span>{{trans('Site.orders.services_purchases')}}  </h4>
                    <div class="content">
                        <p class="title"> {{trans('Site.orders.country_shipping')}}   </p>
                        <img src="{{ request()->root()  }}/public/Site/img/sm.png" height="46" width="123"/></div>
                    <h4><span>4</span>{{trans('Site.orders.payment_orders')}}     </h4>
                    <div class="content">
                        <div class="radio">
                            <label>
                                <input type="radio" value="1" name="payment" checked>
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                <img src="{{ request()->root()  }}/public/Site/img/card-1.png"/>
                                <img src="{{ request()->root()  }}/public/Site/img/card-2.png"/>
                                <img src="{{ request()->root()  }}/public/Site/img/card-3.png"/>
                                <img src="{{ request()->root()  }}/public/Site/img/card-4.png"/>
                                <img src="{{ request()->root()  }}/public/Site/img/card-5.png"/>
                            </label>
                            <p class="payTitle">
                                {{trans('Site.orders.information_static')}}
                            </p>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" value="2" name="payment">
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                {{trans('Site.orders.Paiement_orders_recieving')}}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" value="3" name="payment">
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                {{trans('Site.orders.pay_orders')}}
                            </label>
                        </div>
                        <div class="radio">
                            <label>

                                <input type="radio" value="4" name="payment">
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                <img style="width: 50px ;height: 40px" src="{{ request()->root()  }}/public/Site/img/madablack.png"/>

                                {{trans('Site.orders.bank_card_madaa')}}
                            </label>
                        </div>
                        <button type="submit" id="hiddenButton" class="btn btn-success">{{trans('Site.orders.checkout_now')}}</button>

                    </div>
                </div>
            </div>

            </form>
        </div>
    </div>





@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            var inputAddress = $(".changeCurrentAddress");
            var regionCity = $(".regionCity");
            var inputChecked =  $("#checkedAddress");
            var requiredCity = document.getElementById("cityChoose");



            $( inputChecked ).click(function() {

                if (inputChecked[0].checked  ){

                    $(inputAddress).toggle();
                    $(requiredCity).prop('required',true);
                }else {

                    $(inputAddress).toggle();
//                    $(regionCity).hide();
                    $(requiredCity).prop('required',false);
                }

            });







            $(document).on('change', '.city', function () {

                var id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');

                var myButton = document.getElementById("hiddenButton");
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': id},
                    success:function (data) {

                        if (data == 401){
                            myButton.classList.remove("hidden-category");
                            $("#myCountery").hide();

                        }else {
                            $("#myCountery").show();
                            myButton.classList.add("hidden-category");
                            op += '<option  disabled selected>{{trans('Site.register_details.city')}}</option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }


                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

            $(document).on('click', '.City_chid', function () {

                var Child_City_chid =   $(this).val();
                var myButton = document.getElementById("hiddenButton");
                var myCountery = document.getElementById('myCountery');
                if (Child_City_chid != null || Child_City_chid != undefined ){

//                     $("#myCountery").hide();
                    myButton.classList.remove("hidden-category");
                }

            });








            const  cash = parseInt('{{$dataSetting['payment_cash']}}', 10 )  ;
            var service = Number($("#service")[0].attributes[1].value);
            var totalCart = Number($("#totalCart")[0].attributes[1].value);

           var  servicesAndTotalCart = totalCart + service;

            var totalPrice = $("#totalPrice");
            var totalHidden = $("#totalHidden");

            totalPrice.html(servicesAndTotalCart)

            totalHidden[0].value = servicesAndTotalCart
            console.log(totalHidden[0].value)

            document.getElementById("shippp").innerHTML = 0;

            $('input[type="radio"]').click(function () {


                if(this.value == 2){

                    $("#shippp").html(cash + ' @lang("Site.general.sar")');

                    totalPrice.html(servicesAndTotalCart + cash)
                    totalHidden[0].value = servicesAndTotalCart +cash

                }else {

                    document.getElementById("shippp").innerHTML = 0;

                    totalPrice.html(servicesAndTotalCart )

                }

            });

        });
    </script>


@endsection