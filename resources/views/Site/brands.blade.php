<!-- brands of day -->
<div class="brands text-center">
    <div class="container">
        <div class="col-xs-12">
            <!--content-->
            <div class="content">
                <h1>{{trans('Site.nav.brands')}}</h1>
                {{--<div class="row">--}}
                    {{--<button class="btn btn-warning">show all</button>--}}
                {{--</div>--}}
                <div class="row">


                        <!-- Example single danger button -->
                            <div class="btn-group mobile_select">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    Brands
                                </button>
                                @if(app()->getLocale() == 'en')
                                    <div class="dropdown-menu">
                                    <button class="btn btn-default fil-cat" data-rel="all">All </button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_a">a</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_b">b</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_c">c</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_d">d</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_e">e</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_f">f</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_g">g</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_h">h</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_i">i</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_j">j</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_k">k</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_l">l</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_m">m</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_n">n</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_o">o</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_p">p</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_q">q</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_r">r</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_s">s</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_t">t</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_u">u</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_v">v</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_w">w</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_x">x</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_y">y</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_z">z</button>
                                </div>
                                @else
                                    <div class="dropdown-menu">
                                    <button class="btn btn-default fil-cat" data-rel="letter_ا">ا </button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ب">ب</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ت">ت</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ث">ث</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ج">ج</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ح">ح</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_خ">خ</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_د">د</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ذ">ذ</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ر">ر</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ز">ز</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_س">س</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ش">ش</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ص">ص</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ض">ض</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ط">ط</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ظ">ظ</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ع">ع</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_غ">غ</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ف">ف</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ق">ق</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ك">ك</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ل">ل</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_م">م</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ن">ن</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ه‍">ه‍</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_و">و</button>
                                    <button class="btn btn-default fil-cat" data-rel="letter_ى">ى</button>
                                </div>
                                @endif
                            </div>
                                @if(app()->getLocale() == 'en')
                                    <div class="toolbar pc_select">
                                    <button class="btn fil-cat active" data-rel="all">All</button>
                                    <button class="btn fil-cat" data-rel="letter_a">a</button>
                                    <button class="btn fil-cat" data-rel="letter_b">b</button>
                                    <button class="btn fil-cat" data-rel="letter_c">c</button>
                                    <button class="btn fil-cat" data-rel="letter_d">d</button>
                                    <button class="btn fil-cat" data-rel="letter_e">e</button>
                                    <button class="btn fil-cat" data-rel="letter_f">f</button>
                                    <button class="btn fil-cat" data-rel="letter_g">g</button>
                                    <button class="btn fil-cat" data-rel="letter_h">h</button>
                                    <button class="btn fil-cat" data-rel="letter_i">i</button>
                                    <button class="btn fil-cat" data-rel="letter_j">j</button>
                                    <button class="btn fil-cat" data-rel="letter_k">k</button>
                                    <button class="btn fil-cat" data-rel="letter_l">l</button>
                                    <button class="btn fil-cat" data-rel="letter_m">m</button>
                                    <button class="btn fil-cat" data-rel="letter_n">n</button>
                                    <button class="btn fil-cat" data-rel="letter_o">o</button>
                                    <button class="btn fil-cat" data-rel="letter_p">p</button>
                                    <button class="btn fil-cat" data-rel="letter_q">q</button>
                                    <button class="btn fil-cat" data-rel="letter_r">r</button>
                                    <button class="btn fil-cat" data-rel="letter_s">s</button>
                                    <button class="btn fil-cat" data-rel="letter_t">t</button>
                                    <button class="btn fil-cat" data-rel="letter_u">u</button>
                                    <button class="btn fil-cat" data-rel="letter_v">v</button>
                                    <button class="btn fil-cat" data-rel="letter_w">w</button>
                                    <button class="btn fil-cat" data-rel="letter_x">x</button>
                                    <button class="btn fil-cat" data-rel="letter_y">y</button>
                                    <button class="btn fil-cat" data-rel="letter_z">z</button>
                                </div>
                                @else
                                    <div class="toolbar pc_select">
                                        <button class="btn btn-default fil-cat" data-rel="letter_ا">ا</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ب">ب</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ت">ت</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ث">ث</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ج">ج</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ح">ح</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_خ">خ</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_د">د</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ذ">ذ</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ر">ر</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ز">ز</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_س">س</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ش">ش</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ص">ص</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ض">ض</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ط">ط</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ظ">ظ</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ع">ع</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_غ">غ</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ف">ف</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ق">ق</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ك">ك</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ل">ل</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_م">م</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ن">ن</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ه‍">ه‍</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_و">و</button>
                                        <button class="btn btn-default fil-cat" data-rel="letter_ى">ى</button>
                                    </div>
                                @endif



                    <div id="portfolio">
                        @foreach($brands as $brand)

                            <?php
                            $lastChar = strtolower(mb_substr($brand->name , 0, 1));
                            ?>
                            <div class="tile scale-anm letter_<?= $lastChar; ?> all">
                                <a href="{{route('site.getAllProducts' ).'?brandId='.$brand->id}}">
                                    <div class="overlay">
                                        <p>{{trans('Site.index.show_brand')}}</p>
                                    </div>
                                </a>
                                <img src="{{$brand->image}}" alt=""/>
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
