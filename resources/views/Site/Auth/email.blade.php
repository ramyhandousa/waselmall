@extends('Site.layouts.master')
@section('title', trans('Site.login.login'))



@section('content')

    <div class="login text-center">
        <div class="container">
            <div class="row">

                <form action="{{route('site.sendMail')}}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="col-md-10 col-md-offset-1 col-xs-12">
                        <div class="col-md-12 col-xs-12">
                            <div class="content" style="min-height: 100% !important;">
                                <h1> {{trans('Site.login.reset_email')}}</h1>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder=" {{trans('Site.register_details.email')}} *" required="required">
                                </div>
                                <button type="submit" class="btn btn-default">{{trans('Site.login.reset_email')}}</button>
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>




@endsection