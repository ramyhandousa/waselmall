@extends('Site.layouts.master')
@section('title', trans('Site.login.register'))

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection

@section('content')

    <!-- register -->
    <div class="register text-center">
        <div class="container">
            <div class="row">
                <h1>{{trans('Site.login.register')}}</h1>
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <div class="content">
                        <form method="post" action="{{route('site.registerSite')}}">

                            {{ csrf_field() }}
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" required name="first_name" placeholder="{{trans('Site.register_details.first_name')}} *">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" required name="last_name" placeholder="{{trans('Site.register_details.last_name')}}*">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" required name="phone" placeholder="{{trans('Site.register_details.phone')}} *">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" required name="email" placeholder="{{trans('Site.register_details.email')}} *">
                                </div>

                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <input type="password" class="form-control" required name="password" placeholder="{{trans('Site.register_details.password')}} *">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="confirm_password"
                                           placeholder="{{trans('Site.register_details.confirm_password')}}  *">
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control" required name="day">
                                                <option value="" disabled selected>{{trans('Site.register_details.day')}}  </option>
                                                @for ($i = 1; $i <= 12; $i++)
                                                    <option value="{{ $i }}"  >{{$i}}  </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control" required name="month">
                                                <option value="" disabled selected>{{trans('Site.register_details.month')}}  </option>
                                                @for ($i = 1; $i <= 12; $i++)
                                                    <option value="{{ $i }}"  >{{$i}}  </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control" required name="year">
                                                <option value="" disabled selected>{{trans('Site.register_details.year')}}  </option>
                                                @foreach( array_reverse(range( date('Y'), 1990 ) ) as $i)
                                                    <option value="{{ $i }}"  >{{$i}}  </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" required name="gender">
                                        <option value="" disabled selected>{{trans('Site.register_details.gender')}} </option>
                                        <option value="1"> {{trans('Site.register_details.gender_male')}} </option>
                                        <option value="2"> {{trans('Site.register_details.gender_female')}} </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <p> {{trans('Site.register_details.contact_details')}} </p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="company" placeholder="{{trans('Site.register_details.company')}}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telephone" placeholder="{{trans('Site.register_details.telephone')}} *">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="fax" placeholder="{{trans('Site.register_details.fax')}}  *">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <p> {{trans('Site.register_details.address_details')}} </p>
                                <div class="form-group">
                                    <input type="text" class="form-control" required name="address" placeholder="{{trans('Site.register_details.address')}}  *">
                                </div>

                                <div class="form-group">

                                    <select class="form-control city" required name="city">
                                        <option value="" disabled selected>{{trans('Site.register_details.city')}}  </option>
                                        @foreach( $countery  as $city)
                                            <option value="{{ $city->id }}"  >{{$city->name}}  </option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group" id="myCountery" >

                                    <select class="form-control City_chid" name="city">
                                        <option value="" disabled selected>{{trans('Site.register_details.region')}}  </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" required name="zip" placeholder="{{trans('Site.register_details.zip')}} ">
                                </div>

                            </div>
                            <div class="col-md-8 col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" required value="">
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            {{trans('Site.register_details.text_terms')}}
                                    </label>
                                    {{--<p>{{trans('Site.register_details.text_static')}}   ... <a href="#">( show more )</a> </p>--}}
                                    <p>{{trans('Site.register_details.text_static')}}   ...  </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <a href="#" class="btn btn-success">{{trans('Site.register_details.back')}}  </a>
                                {{--<a href="#" id="hiddenButton" class="btn btn-default">{{trans('Site.register_details.submit')}}  </a>--}}
                                <button  id="hiddenButton" class="btn btn-default">{{trans('Site.register_details.submit')}}  </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('scripts')

    <script>

        $(document).ready(function() {

            $(document).on('change', '.city', function () {

                var id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');

                var myButton = document.getElementById("hiddenButton");
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': id},
                    success:function (data) {

                        if (data == 401){
                            myButton.classList.remove("hidden-category");
                            $("#myCountery").hide();

                        }else {
                            $("#myCountery").show();
                            myButton.classList.add("hidden-category");
                            op += '<option  disabled selected>إختر المدينة</option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }


                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

            $(document).on('click', '.City_chid', function () {

                var Child_City_chid =   $(this).val();
                var myButton = document.getElementById("hiddenButton");
                var myCountery = document.getElementById('myCountery');
                if (Child_City_chid != null || Child_City_chid != undefined ){

//                     $("#myCountery").hide();
                    myButton.classList.remove("hidden-category");
                }

            });

        });


    </script>


@endsection