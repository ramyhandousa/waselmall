@extends('Site.layouts.master')
@section('title', trans('Site.login.login'))



@section('content')

    <div class="login text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="content" style="min-height: 100% !important;">
                            <h1>{{trans('Site.login.reset_email')}}</h1><br>

                            <form action="{{route('site.updatePass')}}" method="post" accept-charset="utf-8">
                                {{ csrf_field() }}
                                <input type="hidden" name="token_reset" value="{{request('token')}}">
                                <div class="form-group">
                                    <input name="password" type="password" id="password" class="form-control"
                                           placeholder="{{trans('Site.profile.new_password')}} " required="">
                                </div>
                                <div class="form-group">
                                    <input name="confirm_password" type="password" id="password-verify" class="form-control"
                                           placeholder="{{trans('Site.register_details.confirm_password')}}" required="">
                                </div>

                                <button class="btn btn-default controlButton" type="submit">{{trans('Site.general.save')}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

<script>
    $(document).ready(function () {

        password = $('#password');
        password2 = $('#password-verify');

        $( password2 ).one( "click", function() {
            showMessage("@lang('Site.login.confirm_pass_valid')");
        });

        $('#password, #password-verify').on('keyup', function () {

            if ($('#password').val() == $('#password-verify').val()) {

                $('.controlButton').show();
            } else

                $('.controlButton').hide();
            });

    });

</script>
@endsection