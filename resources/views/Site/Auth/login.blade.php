@extends('Site.layouts.master')
@section('title', trans('Site.login.login'))



@section('content')


<div class="login text-center">
    <div class="container">
        <div class="row">
            <h1>{{trans('site.login.my_account')}}</h1>
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12">
                    <div class="content">
                        <h1>{{trans('site.login.login')}}     </h1>
                        <p>{{trans('Site.login.registered_persons')}} </p>
                        <p>{{trans('Site.login.text_static_login')}}</p>
                        <form method="post" action="{{route('site.login')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="{{trans('Site.register_details.email')}} *">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password"
                                       placeholder="{{trans('Site.register_details.password')}} *">
                            </div>
                            <a href="{{route('site.resetEmail')}}"><p>{{trans('site.login.forget_pass')}}</p></a>
                            <br>
                            <button type="submit" class="btn btn-default">{{trans('site.login.login')}}</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="content">
                        <h1>{{trans('Site.login.register')}}</h1>
                        <p>{{trans('Site.login.registered_persons')}}  </p>
                        <p>{{trans('Site.login.text_static_register')}}  .</p>
                        <a href="{{route('site.get.register')}}" class="btn btn-default">  {{trans('Site.login.login')}} </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection