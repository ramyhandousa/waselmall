<!-- top_menu -->
<div id="menu" class="menu">
    <!-- logo -->
    <div class="col-md-2 col-xs-12">
        <div class="logo">
            <img src="{{ request()->root()  }}/public/Site/img/logo.png"/>
        </div>
    </div>
    <!-- menu -->
    <div class="col-md-10 col-xs-12">
        <!--first menu-->
        <nav class="navbar menu">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{trans('site.nav.index')}}</a>
                </div>

                <div class="col-md-11 col-xs-12">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="{{route('firstPageSite')}}">{{trans('site.nav.index')}}  <span class="sr-only">(current)</span></a></li>
                            @if(Auth::check())
                            <li><a href="{{route('usersSite.show',Auth::id())}}">{{trans('site.nav.account')}}</a></li>
                            @endif
                            <li><a href="{{route('site.aboutUs')}}">{{trans('site.nav.about')}}</a></li>
                            <li><a href="{{route('site.contactUs')}}">{{trans('site.nav.contacts')}}</a></li>
                            <li><a href="{{route('site.privacy')}}">{{trans('site.general.privacy')}}</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ request()->root()  }}/public/Site/img/user.png"></a>
                                <ul class="dropdown-menu">
                                    @if(Auth::check())
                                    <li><b>{{auth()->user()->fullname}}</b></li>
                                    <li><a href="{{route('usersSite.show',Auth::id())}}">{{trans('site.nav.profile')}}</a></li>
                                    <li><a href="{{route('site.listFavourite')}}">{{trans('site.nav.my_wishlist')}}</a></li>
                                    {{--<li><a href="account.html">{{trans('site.nav.index')}}my coupons <span>1</span></a></li>--}}
                                    <li><a href="{{route('site.logOut')}}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('site.nav.login_out')}}
                                        </a>
                                    </li>
                                        <form id="logout-form" action="{{ route('site.logOut') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    @else
                                        <li><a href="{{route('site.get.login')}}">{{trans('site.nav.loginOrRegister')}}</a></li>
                                    @endif
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ request()->root()  }}/public/Site/img/cart.png">{{trans('site.cart.items')}}  @if(count($cartUserSite) > 0 ) ( {{count($cartUserSite)}} ) @else [0] @endif </a>
                                <div class="dropdown-menu items">
                                    @if(count($cartUserSite) > 0)
                                        <h3>  {{trans('Site.cart.items_cat' ,['count' => count($cartUserSite) ])}}</h3>
                                        <div class="allItems">
                                            @foreach($cartUserSite as $cart)
                                                <a href="{{route('site.product.show')}}?id={{$cart->product_id}}">
                                                    <div class="col-xs-12">
                                                        <div class="col-md-4 col-xs-12">
                                                            @if($cart['product_filter']->images[0])
                                                            <img src="{{\App\uploadImages::whereId($cart['product_filter']->images[0])->first()->url}}" class="img-responsive"/>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-7 col-md-offset-1 col-xs-12">
                                                            <h4>{{$cart['product_filter']->name}}</h4>
                                                            <p>{{$cart['product_filter']->category->name}}</p>
                                                            <p>{{trans('Site.index.quantity')}}  {{$cart->quantity}}</p>
                                                            <h4>{{trans('Site.index.price')}} {{$cart->price}}</h4>
                                                        </div>

                                                    </div>
                                                </a>
                                            @endforeach

                                        </div>
                                        <div class="clearfix"></div>
                                        <h4>{{trans('Site.cart.total')}} :  {{$countCartUserSite}}</h4>
                                        <a href="{{route('site.getAllOrders')}}" class="btn btn-default">{{trans('site.nav.checkout_now')}} </a>
                                        <a href="{{route('cartSite.show',\App\Models\Cart::getIdCart(Auth::user()) )}}" class="btn btn-default">{{trans('site.cart.pastOrders')}}</a>
                                    @else
                                        {{trans('Site.cart.empty')}}
                                    @endif
                                </div>
                            </li>
                            @foreach (config('translatable.locales') as $lang => $language)
                                @if ($lang != app()->getLocale())
                                    <li>
                                        <a href="{{ route('lang.switch', $lang) }}">
                                            {{ $language }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <!--second menu-->
        <nav class="navbar category">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{trans('site.nav.categories')}} </a>
                </div>
                <div class="col-md-11 col-xs-12">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <ul class="nav navbar-nav">
                            @foreach($categoriesSite as $category)

                                <li>

                                    <a href="{{route('site.getAllProducts')}}?id={{$category->id}}">{{$category->name}}</a>

                                </li>
                            @endforeach

                        </ul>
                        <form method="Get" action="{{ route('site.search')}}" class="navbar-form navbar-right">
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" placeholder="{{trans('site.nav.search_for')}}">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><img src="{{ request()->root()  }}/public/Site/img/search.png"></button>
                              </span>
                            </div><!-- /input-group -->
                        </form>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>

<div class="clearfix"></div>