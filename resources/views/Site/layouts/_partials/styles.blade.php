

{{--<link href="{{ request()->root() }}/public/assets/admin/css-{{ config('app.locale') }}/bootstrap.min.css" rel="stylesheet"--}}
      {{--type="text/css"/>--}}

<link href="{{ Request::root() }}/public/assets/admin/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ request()->root()  }}/public/assets/admin/plugins/parsleyjs/src/parsley.css" rel="stylesheet" type="text/css" />

<!-- Sweet Alert css -->
<link href="{{ request()->root() }}/public/assets/admin/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet"
      type="text/css"/>

<!-- icon -->
<link href="{{ request()->root()  }}/public/Site/img/logo.ico" rel="icon">
<!-- css -->
<link href="{{ request()->root()  }}/public/Site/css/bootstrap_{{ config('app.locale') }}.min.css" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/bootstrap.css.map" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/bootstrap-arabic.css.map" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/bootstrap-theme.css" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/media.css" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ request()->root()  }}/public/Site/css/owl.carousel.min.css" rel="stylesheet"/>
<link href="{{ request()->root()  }}/public/Site/css/owl.theme.default.min.css" rel="stylesheet"/>
<link href="{{ request()->root()  }}/public/Site/css/jquery.countdownTimer.css" rel="stylesheet"/>
<link href="{{ request()->root()  }}/public/Site/css/style_{{ config('app.locale') }}.css" rel="stylesheet">

