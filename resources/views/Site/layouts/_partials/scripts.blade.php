<!--begin:: Global Mandatory Vendors -->
<script src="{{ request()->root()  }}/public/Site/js/jquery-3.2.1.min.js" ></script>
{{--<script src="https://code.jquery.com/jquery-2.0.3.min.js"--}}
        {{--integrity="sha256-sTy1mJ4I/LAjFCCdEB4RAvPSmRCb3CU7YqodohyeOLo="--}}
        {{--crossorigin="anonymous"></script>--}}
{{--<script src="{{ request()->root()  }}/public/Site/js/jquery.1.11.1.min.js" type="text/javascript"></script>--}}
<script src="{{ request()->root()  }}/public/Site/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.nicescroll.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.elevatezoom.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.countdown.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.countdownTimer.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.images-rotation.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/respond.min.js" type="text/javascript"></script>
<script src="{{ request()->root()  }}/public/Site/js/main.min.js" type="text/javascript"></script>


<script src="{{ request()->root()  }}/public/assets/admin/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript"  src="{{ request()->root()  }}/public/assets/admin/plugins/parsleyjs/dist/parsley.min.js"></script>

<script src="{{ request()->root() }}/public/assets/admin/js/validate-ar.js"></script>
<!-- Sweet Alert js -->
<script src="{{ request()->root() }}/public/assets/admin/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="{{ request()->root() }}/public/assets/admin/pages/jquery.sweet-alert.init.js"></script>
<script src="{{ request()->root()  }}/public/Site/js/jquery.tinytoggle.min.js"></script>
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>--}}


