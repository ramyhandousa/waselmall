
<!-- footer -->
<footer>
    <div class="logo-footer">
        <img src="{{ request()->root()  }}/public/Site/img/logo.png">
    </div>
    <div class="content">
        <a href="#menu" class="arrowTop smoothscroll"><i class="fa fa-angle-up"></i> </a>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <p>24/ 7</p>
                    <nav>
                        <a href="#">about</a>
                        <span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                        <a href="#">contact us</a>
                    </nav>
                </div>
                <div class="col-md-4 col-xs-12 text-center">
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/facebook.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/twitter.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/youtube.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/google.png"></a>
                    <p> {{trans('Site.footer.footer_text')}} </p>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>

                        <input type="text" class="form-control" placeholder="{{trans('Site.search.search_text')}}...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-send-o"></i> </button>
                              </span>
                    </div><!-- /input-group -->
                </div>
                <div class="col-md-3 col-md-push-1 col-xs-12">
                    <p>Saudi Arabia , LA854900</p>
                    <p>{{trans('Site.contact.email')}}: {{$setting->where('key','contactus_email')->first() ?$setting->where('key','contactus_email')->first()->body : ''  }}</p>
                    <p>{{trans('Site.contact.phone')}}: {{$setting->where('key','contactus_phone')->first() ?$setting->where('key','contactus_phone')->first()->body : ''  }}</p>


                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/card-1.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/card-2.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/card-3.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/card-4.png"></a>
                    <a href="#"><img src="{{ request()->root()  }}/public/Site/img/card-5.png"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright text-center">
        <p>{{trans('Site.footer.copyright')}} </p>
    </div>
</footer>
