<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title-->
    <title>  @yield('title')</title>

    @include('Site.layouts._partials.styles')
    @yield('styles')
    <style>

    </style>

</head>
<!-- begin::Body -->
<body  >


    @include('Site.layouts._partials.header')



        @yield('content')
    {{--<div id="countdowntimer">--}}
        {{--<span id="future_date"></span>--}}
    {{--</div>--}}



@include('Site.layouts._partials.footer')

@include('Site.layouts._partials.scripts')



<script>


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    @if(session()->has('success'))
        setTimeout(function () {
        showMessage('{{ session()->get('success') }}');
    }, 1000);

    @endif

    function showMessage(message) {

        var shortCutFunction = 'success';
        var msg = message;

        var title = "@lang('Site.general.success')";
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }


    @if(session()->has('warning'))
        setTimeout(function () {
        showWarning('{{ session()->get('warning') }}');
    }, 1000);

    @endif

    function showWarning(message) {

        var shortCutFunction = 'warning';
        var msg = message;
        var title = 'تحذير!';
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }

    @if(session()->has('errors'))
        setTimeout(function () {
        showErrors('{{ session()->get('errors') }}');
    }, 1000);

    @endif

    function showErrors(message) {

        var shortCutFunction = 'error';
        var msg = message;
        var title = "@lang('Site.general.error')";
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }

    @if(session()->has('validation'))
        setTimeout(function () {
        showValidation('{{ session()->get('validation') }}');
    }, 1000);

    @endif

    function showValidation(message) {

        var shortCutFunction = 'warning';
        var msg = message;
        var title = 'مراحعة البيانات!';
        toastr.options = {
            positionClass: 'toast-top-center',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;

    }



</script>

<script>
    //        date count down
    $(document).ready(function () {
        $(".getting-started").each(function (counter, item) {
            var date = $(this).attr('data-date');

            $(this).countdowntimer({
                dateAndTime : date,
                size : "xs",
                regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
//                regexpReplaceWith: "$1:days:/ $2:hours / $3:minutes  / $4:seconds"
                regexpReplaceWith: "$1:days:/ $2:H / $3:M /$4:S"
            });
//            $(this).countdown(date, function (event) {
//
//                $(this).text(
//
//                    event.strftime('%DDays  :  %HH   %MM   %SS')
//                );
//            })
        });

    });





</script>

@yield('scripts')


</body>

<!-- end::Body -->
</html>