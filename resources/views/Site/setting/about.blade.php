@extends('Site.layouts.master')
@section('title', trans('Site.general.home'))

@section('content')



    <!-- about -->
    <div class="about">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <h3>{{ trans('Site.general.vision') }}</h3>

                    <h5> {{$dataSetting['vision']}} </h5>

                </div>
                <div class="col-md-4 col-xs-12">
                    <h3>{{ trans('Site.general.mission') }}</h3>

                    <h5> {{$dataSetting['mission']}} </h5>

                </div>

                <div class="col-md-4 col-xs-12">
                    <h3>{{ trans('Site.general.rate_us') }}</h3>

                    <h5> {{$dataSetting['rate_us']}} </h5>

                </div>

            </div>
        </div>

    </div>



@endsection