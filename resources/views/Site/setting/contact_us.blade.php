@extends('Site.layouts.master')
@section('title', trans('Site.general.home'))

@section('content')


    <!-- contact -->
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="details text-center">
                        <h4><i class="fa fa-map-marker"></i> </h4>
                        <p>
                            {{trans('Site.contact.location')}}
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="details text-center">
                        <h4><i class="fa fa-phone-square"></i> </h4>
                        <p>
                            {{$dataSetting['phone']}}
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="details text-center">
                        <h4><i class="fa fa-envelope"></i> </h4>
                        <p>
                            {{$dataSetting['email']}}
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13675.309964500439!2d31.359880999999994!3d31.031055600000002!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1494859269406" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-xs-12 text-center">
                    <h1>{{trans('Site.contact.title')}}</h1>
                </div>
            </div>
            <div class="row">
                <form method="post" action="{{route('site.contactUs')}}">
                    {{ csrf_field() }}
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label for="your_name"> {{trans('Site.contact.name')}}  </label>
                            <input type="text" class="form-control" name="name" required id="your_name" placeholder="{{trans('Site.contact.name')}} ">
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label for="your_email">{{trans('Site.contact.email')}}</label>
                            <input type="email" class="form-control" name="email" required id="your_email" placeholder="{{trans('Site.contact.email')}}">
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label for="subject">{{trans('Site.contact.address')}}</label>
                            <input type="text" class="form-control" name="address" required id="subject" placeholder="{{trans('Site.contact.address')}}">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="your_message">{{trans('Site.contact.text')}}</label>
                            <textarea class="form-control" name="message" id="your_message"  placeholder="{{trans('Site.contact.text')}}" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-default">{{trans('Site.contact.submit')}} </button>
                    </div>
                </form>
            </div>
        </div>

    </div>


@endsection