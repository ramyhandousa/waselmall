@extends('admin.layouts.master')
@section('title' , 'إضافة مستخدم')

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')
    <form id="myForm" method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data" data-parsley-validate
          novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة الشركات</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">إضافة شركة</h4>
                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="userName">الاسم الأول*</label>
                            <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control"
                                   required
                                   placeholder="اسم المستخدم الأول..."
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="3"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 3 حروف "
                                   data-parsley-required-message="يجب ادخال  اسم المستخدم"

                            />
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('first_name'))
                                <p class="help-block">
                                    {{ $errors->first('first_name') }}
                                </p>
                            @endif
                        </div>

                    </div>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="userName">الاسم الأخير*</label>
                            <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control"
                                   required
                                   placeholder="اسم المستخدم الأخير..."
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="3"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 3 حروف "
                                   data-parsley-required-message="يجب ادخال  اسم المستخدم"

                            />
                            <p class="help-block" id="error_userName"></p>
                            @if($errors->has('last_name'))
                                <p class="help-block">
                                    {{ $errors->first('last_name') }}
                                </p>
                            @endif
                        </div>

                    </div>


                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="userPhone">رقم الجوال*</label>
                            <input type="number" name="phone" value="{{ old('phone') }}" class="form-control"
                                   required
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الاسم  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="5"
                                   data-parsley-minLength-message=" الاسم  يجب أن يكون اكثر من 5 حروف "
                                   data-parsley-required-message="يجب ادخال رقم الجوال"
                                   placeholder="رقم الجوال..."
                            />
                            @if($errors->has('phone'))
                                <p class="help-block">
                                    {{ $errors->first('phone') }}
                                </p>
                            @endif
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="emailAddress">البريد الإلكتروني*</label>

                            <input type="email" name="email" parsley-trigger="change" value="{{ old('email') }}"
                                   class="form-control"
                                   placeholder="البريد الإلكتروني..." required
                                   data-parsley-type="email"
                                   data-parsley-type-message="أدخل البريد الالكتروني بطريقة صحيحة"
                                   data-parsley-required-message="يجب ادخال  البريد الالكتروني"
                                   data-parsley-maxLength="30"
                                   data-parsley-maxLength-message=" البريد الالكتروني  يجب أن يكون ثلاثون حرف فقط"
                                   {{--data-parsley-pattern="/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm"--}}
                                   {{--data-parsley-pattern-message="أدخل  البريد الالكتروني بطريقة الايميل ومن غير مسافات"--}}
                                   required
                            />
                            @if($errors->has('email'))
                                <p class="help-block">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="pass1">كلمة المرور*</label>
                            <input type="password" name="password" id="pass1" value="{{ old('password') }}"
                                   class="form-control"
                                   placeholder="كلمة المرور..."
                                   required
                                   data-parsley-required-message="هذا الحقل مطلوب"
                                   data-parsley-maxLength="20"
                                   data-parsley-maxLength-message=" الباسورد  يجب أن يكون عشرون حروف فقط"
                                   data-parsley-minLength="5"
                                   data-parsley-minLength-message=" الباسورد  يجب أن يكون اكثر من 5 حروف "
                            />

                            @if($errors->has('password'))
                                <p class="help-block">{{ $errors->first('password') }}</p>
                            @endif

                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="passWord2">تأكيد كلمة المرور*</label>
                            <input data-parsley-equalto="#pass1" name="password_confirmation" type="password" required
                                   placeholder="تأكيد كلمة المرور..." class="form-control" id="passWord2"
                                   data-parsley-required-message="تأكيد كلمة المرور مطلوب"
                                   data-parsley-maxlength="55"
                                   data-parsley-minlength="8"
                                   data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (55) حرف"
                                   data-parsley-minlength-message=" أقل عدد الحروف المسموح بها هى (8) حرف"
                                   data-parsley-equalto="#pass1"
                                   data-parsley-equalto-message ='غير مطابقة لكلمة المرور'
                            >
                            @if($errors->has('password_confirmation'))
                                <p class="help-block">{{ $errors->first('password_confirmation') }}</p>
                            @endif


                        </div>
                    </div>


                    <div class="form-group">
                        <label for="passWord2">العنوان*</label>
                        <input name="address" value="{{ old('address') }}" type="text" placeholder="العنوان..."
                            class="form-control"
                               data-parsley-maxlength="191"
                               data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (191) حرف"
                        >

                    </div>

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">النوع  *</label>
                            <select   class="form-control"  name="gender"  required  data-parsley-required-message="من فضلك اختار نوع الشخص "   >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                <option value="1" >ذكر </option>
                                <option value="2" >أنثي</option>
                            </select>

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">الماركات *</label>
                            <select   class="form-control"  name="brand"
                                      required  data-parsley-required-message="من فضلك اختار ماركة على الاقل"  >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                @foreach($brands as  $value)
                                    <option value="{{ $value->id }}" >{{ $value->name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <label for="passWord2">الدولة *</label>
                            <select   class="form-control city"  name="city"
                                      required  data-parsley-required-message="من فضلك اختار الدولة"  >
                                <option value="" disabled selected hidden class="text-white">إختر </option>
                                @foreach($cities as  $value)
                                    <option value="{{ $value->id }}" >{{ $value->name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="col-xs-6" id="myCountery"  >
                        <div class="form-group ">
                            <label for="passWord2">المدينة *</label>
                            <select   class="form-control City_chid"  name="city"
                                      {{--required  data-parsley-required-message="من فضلك اختار مدينة  "  --}}
                            >
                                <option value="" disabled selected hidden class="text-white">إختر </option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group text-right m-t-20">
                        <button id="hiddenButton" class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            حفظ البيانات
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            إلغاء
                        </button>
                    </div>

                </div>
            </div><!-- end col -->

            <div class="col-lg-4">
                <div class="card-box" style="overflow: hidden;">
                    <h4 class="header-title m-t-0 m-b-30">الصورة الشخصية</h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input data-parsley-fileextension='jpg,png' id="image" type="file" accept='image/*' name="image" class="dropify" data-max-file-size="6M"/>

                        </div>
                    </div>

                    <span class="help-block">
	                	<strong hidden id='error' style="color: red;">الصورة يجب ان تكون بصيغة PNG او JPG</strong>
	            	</span>


                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection

@section('scripts')

    <script>

        $(document).ready(function() {

            $(document).on('change', '.city', function () {

                var id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');

                var myButton = document.getElementById("hiddenButton");
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': id},
                    success:function (data) {

                        if (data == 401){
                            myButton.classList.remove("hidden-category");
                            $("#myCountery").hide();

                        }else {
                            $("#myCountery").show();
                            myButton.classList.add("hidden-category");
                            op += '<option  disabled selected>إختر المدينة</option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }


                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

            $(document).on('click', '.City_chid', function () {

                var Child_City_chid =   $(this).val();
                var myButton = document.getElementById("hiddenButton");
                var myCountery = document.getElementById('myCountery');
                if (Child_City_chid != null || Child_City_chid != undefined ){

//                     $("#myCountery").hide();
                    myButton.classList.remove("hidden-category");
                }

            });

        });


    </script>


@endsection
