@extends('admin.layouts.master')

@section('content')


    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">بيانات الطلب</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                <div class="row">
                    <h4 class="header-title m-t-0 m-b-30"> بيانات </h4>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">رقم  الطلب*</label>
                            <input type="text" name="name" value="{{ $order->id  }}" class="form-control"
                                   readonly
                                   placeholder="اسم المستخدم بالكامل..."/>

                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userName">إسم المستخدم*</label>
                            <input type="text" name="name" value="{{  optional($order->user)->name   }}" class="form-control"
                                   readonly
                                   placeholder="اسم المستخدم بالكامل..."/>

                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">إسم السائق  *</label>
                            <input type="text" name="phone" value="{{  optional($order->driver)->name  }}"
                                   class="form-control" readonly
                                   placeholder="رقم الجوال..."/>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">مكان  الطلب  *</label>
                            <input type="text" name="phone" value="{{ $order->address  }}"
                                   class="form-control" readonly
                                   placeholder="رقم الجوال..."/>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">حالة  الطلب  *</label>
                            <input type="text" name="phone" value="{{ $order->status  }}"
                                   class="form-control" readonly
                                   placeholder="رقم الجوال..."/>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="userPhone">سعر  الطلب  *</label>
                            <input type="text" name="phone" value="{{ $order->total_price  }}"
                                   class="form-control" readonly
                                   placeholder="رقم الجوال..."/>
                        </div>
                    </div>
                </div>



                </div>

            </div>


        </div>
        <!-- end row -->


@endsection

