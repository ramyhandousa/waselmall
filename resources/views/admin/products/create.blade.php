@extends('admin.layouts.master')
@section('title', __('maincp.users_manager'))


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">


@endsection
@section('content')



    <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30"> إضافة {{$pageName}}</h4>

                    <div class="row">


                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">  إختر الشركة للمنتج  *</label>
                                <select name="userId" id="" class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled=""> إختر الشركة</option>
                                    @foreach($users as $value)
                                        <option value="{{ $value->id }}">{{ $value->fullname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">إسم القسم الرئيسي*</label>
                                <select name="categoryId"  class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled=""> إختر القسم</option>

                                    @foreach($categories as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">  الماركة*</label>
                                <select name="brand"  class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled=""> إختر ماركة</option>
                                    @foreach($brands as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">العملة *</label>
                                <select name="currency"  class="form-control requiredFieldWithMaxLenght" required>
                                    <option value="" selected disabled=""> إختر العملة</option>
                                    @foreach($currencies as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">المقاسات*</label>
                                <select class="js-example-basic-multiple requiredFieldWithMaxLenght" required
                                        name="sizes[]" multiple="multiple" data-placeholder="إختر ...">
                                    <option value="" selected disabled=""> إختر مقاس</option>
                                    @foreach($sizes as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">الألوان*</label>
                                <select class="js-example-basic-multiple requiredFieldWithMaxLenght" required
                                        name="colors[]" multiple="multiple" data-placeholder="إختر ...">
                                    <option value="" selected disabled=""> إختر لوان</option>
                                    @foreach($colors as $value)
                                        <option  value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">الخصومات علي المنتجات *</label>

                                <select class="js-example-basic-multiple  "
                                        name="purchaseType[]" multiple="multiple" data-placeholder="إختر ...">
                                    <option value="" selected disabled=""> تابعه ل</option>
                                    @foreach($purchaseType as $value)
                                        <option  value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">سعر المنتج *</label>
                                <input type="number" name="price"  class="form-control requiredFieldWithMaxLenght" required>

                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">الكمية المتوفرة من المنتج  *</label>
                                <input type="number" name="quantity"  class="form-control requiredFieldWithMaxLenght" required>

                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group col-lg-6 ">
                                <label>هل يوجد خصم؟</label>
                                <input type="checkbox" name="isOffer" value="1"  />
                            </div>
                            <div class="form-group col-lg-6 priceOffer" style="display: none">
                                <label>    سعر الخصم </label>
                                <input type="text" name="priceOffer" value=""  class="form-control" />
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <label>هل يوجد عرض؟</label>
                                <input type="checkbox" name="dealOfToday" value="1"  class="styled" />
                            </div>
                            <div class="form-group col-lg-6 dealField" style="display: none">

                                <label>تاريخ انتهاء العرض</label>
                                <input type="text" name="dealDate" value=""  class="form-control datepicker readonly" />

                            </div>
                        </div>



                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}}   باللغة العربية</label>
                                <input type="text" name="name_ar"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة العربية{{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}}   باللغة الإنجليزية</label>
                                <input type="text" name="name_en"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة الإنجليزية {{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_en'))
                                    <p class="help-block">
                                        {{ $errors->first('name_en') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}} وصف باللغة العربية</label>
                                <textarea type="text" name="description_ar" class="form-control m-input requiredFieldWithMaxLenght" required
                                          placeholder="إدخل  وصف عن  المنتج العربية   "   ></textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('description_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('description_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName"> {{$pageName}}  وصف باللغة الإنجليزية</label>
                                <textarea type="text" name="description_en" class="form-control m-input requiredFieldWithMaxLenght" required
                                          placeholder="إدخل  وصف عن  المنتج بالإنجليزي   "   ></textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('description_en'))
                                    <p class="help-block">
                                        {{ $errors->first('description_en') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">

                            @for ($i = 0; $i < 4; $i++)
                                <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="usernames">صورة المنتج  </label>
                                    <input type="file" name="image[]" class="dropify" data-max-file-size="6M"/>
                                </div>
                                </div>
                            @endfor

                        </div>



                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {




            $('.js-example-basic-multiple').select2();

        });

        $('input[name=isOffer]').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.priceOffer').show();
            } else {
                $('.priceOffer').hide();
            }
        });

        $('input[name=dealOfToday]').on('click', function (e) {

            if ($(this).is(':checked')) {
                $('.dealField').show();
            } else {
                $('.dealField').hide();
            }
        });

        $( '.styled' ).one( "click", function() {
            var shortCutFunction = 'success';
            var msg = 'سوف يتم معاملة العرض علي انه من ضمن العروض اليومية ';
            var title = 'نجاح';
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;
        });




        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()){
                $('.loading').show();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        // $('form').trigger("reset");
                        console.log(data);

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

