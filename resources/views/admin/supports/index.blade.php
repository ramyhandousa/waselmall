@extends('admin.layouts.master')

@section('title', __('maincp.call_us'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

                {{--<a h --}}

                <button type="button" class="btn btn-custom  waves-effect waves-light"
                        onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                </button>




            </div>

            <h4 class="page-title">@lang('maincp.call_us')</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="dropdown pull-right">
                </div>

                <h4 class="header-title m-t-0 m-b-30">@lang('maincp.contact_us')</h4>

                <table class="table m-0  table-striped table-hover table-condensed" id="datatable-fixed-header">
                    <thead>
                    <tr>
                        <th>
                            @lang('maincp.type_of_sender')
                        </th>

                        <th>@lang('maincp.mobile_number') </th>

                        <th>@lang('trans.isRead') </th>
                        <th>@lang('maincp.date_of_the_message') </th>
                        <th>@lang('maincp.choose') </th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach($supports as $row)
                        <tr>
                            <td>

                                {{ optional($row->user)->fullname ?: 'لا يوجد اسم ' }}

                            </td>

                            <td>
                                @if($row->user != '')
                                    {{ ($row->user->phone)?:'--' }}
                                @else
                                    {{ $row->phone }}
                                @endif
                            </td>

                            <td>
                                @if($row->is_read == 1)
                                <div>
                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">
                                </div>
                                @else
                                <div >
                                    <img width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">
                                </div>

                                @endif

                            </td>

                            <td>{{ $row->created_at->format('F Y d') }}</td>
                            <td>
                                <a href="{{ route('support.show', $row->id) }}"
                                   class="btn btn-icon btn-xs waves-effect btn-info m-b-5">
                                    <i class="fa fa-reply"></i>
                                </a>
                                <a href="javascript:;" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}" data-url="{{ route('support.contact.delete') }}"
                                   class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


                {{--<div class="articles">--}}

                {{--                    @include('admin.categories.load')--}}

                {{--</div>--}}
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->



@endsection


@section('scripts')


    <script>


        @if(session()->has('success'))

        setTimeout(function () {
            showMessage('{{ session()->get('success') }}');
        }, 3000);


        @endif



        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var $url = $(this).attr('data-url');

            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('support.contact.delete') }}',
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                            }

                            $tr.find('td').fadeOut(1000, function () {
                                $tr.remove();
                            });
                        }
                    });
                } else {

                    swal({
                        title: "تم الالغاء",
                        text: "انت لغيت عملية الحذف تقدر تحاول فى اى وقت :)",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "موافق",
                        confirmButtonClass: 'btn-info waves-effect waves-light',
                        closeOnConfirm: false,
                        closeOnCancel: false

                    });

                }
            });
        });


        function showMessage(message) {

            var shortCutFunction = 'success';
            var msg = message;
            var title = 'نجاح!';
            toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",
            };
            var $toast = toastr[shortCutFunction](msg, title);
            // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;


        }
    </script>



@endsection


