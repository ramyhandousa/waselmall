@extends('admin.layouts.master')

@section('title', __('maincp.call_us'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

                <a href="{{ route('support.index') }}"
                   style="margin: 0 15px"
                   class="btn btn-success ">
                    رجوع إلي رسائل تواصل معنا
                </a>


            </div>

            <h4 class="page-title">@lang('maincp.call_us')</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="dropdown pull-right">
                </div>

                <h4 class="header-title m-t-0 m-b-30">نوع الرسالة</h4>

                <table class="table m-0  table-striped table-hover table-condensed" id="datatable-fixed-header">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>إسم الرسالة</th>
                        <th>  الخيارات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($types as $type)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{ $type->name }} </td>
                            <td>
                                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#con-close-modal{{$type->id}}">  <i class="fa fa-pencil"></i></button>
                                <div id="con-close-modal{{$type->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">    تعديل نوع الرسالة   </h4>
                                            </div>
                                            <form method="POST" action="{{ route('types.update', $type->id) }}" >
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label   class="control-label">تعديل :</label>
                                                                <input type="text" class="  form-control" name="name_ar" value="{{$type->name}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">إلغاء</button>
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">حفظ  </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.modal -->

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>




                {{--<div class="articles">--}}

                {{--                    @include('admin.categories.load')--}}

                {{--</div>--}}
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->



@endsection


@section('scripts')


    <script>






        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var $url = $(this).attr('data-url');

            swal({
                title: "هل انت متأكد؟",
                text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: $url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                            }

                            $tr.find('td').fadeOut(1000, function () {
                                $tr.remove();
                            });
                        }
                    });
                } else {

                    swal({
                        title: "تم الالغاء",
                        text: "انت لغيت عملية الحذف تقدر تحاول فى اى وقت :)",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "موافق",
                        confirmButtonClass: 'btn-info waves-effect waves-light',
                        closeOnConfirm: false,
                        closeOnCancel: false

                    });

                }
            });
        });

    </script>



@endsection


