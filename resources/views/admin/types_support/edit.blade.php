@extends('admin.layouts.master')

@section('title','الشكوي')

@section('styles')
    <style>
        .help-block {
            color: #ff5b5b !important;
            font-size: 16px !important;
        }
        .validation_style {
            border: 1px solid red !important;
        }
    </style>
@endsection
@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-content">

            <div class="row">
                <div class="col-lg-12">

                    <!--begin::Portlet-->
                    <div class="m-portlet">

                        <div class="m-portlet__head">

                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">

                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        تعديل الشكوي
                                    </h3>

                                </div>

                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a href="{{route('types.index')}}"
                                           class="btn btn-metal">
												<span>
													<i class="la la-backward"></i>
													<span>رجوع  </span>
												</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>

                        </div>

                        <form method="POST" action="{{ route('types.update', $type->id) }}" enctype="multipart/form-data"
                              data-parsley-validate novalidate>

                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">الشكوي :</h3>
                                    </div>

                                    <div class="form-group m-form__group row{{ $errors->has('name_ar') ? ' has-error' : '' }}">
                                        <label class="col-lg-2 col-form-label">اسم الرسالة  *:</label>
                                        <div class="col-lg-8">
                                            <input type="text" name="name_ar"
                                                        class="form-control m-input nameValid"
                                                        value=" {{ $type->name }}"
                                                   required
                                                   placeholder="إدخل اسم المدينة باللغة العربية   "   >
                                        </div>
                                        @if($errors->has('name_ar'))
                                            <p class="help-block">
                                                {{ $errors->first('name_ar') }}
                                            </p>
                                        @endif
                                    </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions">
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-6">
                                            <button type="submit" class="btn btn-primary" id="m_blockui_3_1">تعديل </button>
                                            <a href="{{route('types.index')}}" type="reset" class="btn btn-secondary">الغاء</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>



                        <!--end::Form-->
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection



