@component('mail::layout')

    @slot('header')
        @component('mail::header', ['url' => route("site.updatePassword").'?token='.$user->token_reset])
Click Here !
        @endcomponent
    @endslot

    Hello ,  {{$user->fullname}}.
    We heard you need a password reset.
    Click the link below and you'll be redirected to a secure site from which you can set a new password


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} واصل مول.
        @endcomponent
    @endslot
@endcomponent
