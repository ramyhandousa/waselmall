<?php

namespace App;

use App\Models\Ad;
use App\Models\Brand;
use App\Models\City;
use App\Models\Conversation;
use App\Models\Countery;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Product;
use App\Models\VerifyUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;
use willvincent\Rateable\Rateable;
use willvincent\Rateable\Rating;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities;
    use  Rateable;
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
        
    }
    
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'gender',
        'city_id',
        'brand_id',
        'password',
        'image',
        'address',
        'zip_code',
        'company_name',
        'company_phone',
        'company_fax',
        'data_of_birth',
        'is_active',
        'is_accepted',
        'is_suspend',
        'message',
        'login_count',
        'token_reset',
    ];

    
    protected $hidden = [
        'password','remember_token'
    ];


    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function hasAnyRoles()
    {
        if (auth()->check()) {
            
            
            if (auth()->user()->roles->count()) {
                return true;
            }
            
        } else {
            redirect(route('admin.login'));
        }
    }
    
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function code()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }


   
    public function ratings()
    {
        return $this->morphMany('willvincent\Rateable\Rating', 'rateable');
    }



    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }


    public function my_favorites()
    {

        return $this->belongsToMany(Product::class, 'favorite_user', 'user_id', 'product_id')->withTimestamps();

    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }



}
