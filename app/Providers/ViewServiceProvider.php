<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;

use App\Libraries\Main;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
 
            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $main = new Main();

            $categoriesSite = Category::whereParentId(0)->where('is_suspend',0)->get();


            if (\Auth::check()){
                $cartUserSite =  Cart::getCart(\Auth::user());
                $countCartUserSite = Cart::getTotalPrice(\Auth::user());
            }else{
                $cartUserSite = [];
                $countCartUserSite = 0;
            }



            $view->with(
                compact('categoriesSite','cartUserSite','countCartUserSite',
                'helper', 'main', 'setting','main_helper','cities', 'branches')
            );
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


