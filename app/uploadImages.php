<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class uploadImages extends Model
{
       protected $table = 'upload_images';
    protected $fillable = ['url'];
}
