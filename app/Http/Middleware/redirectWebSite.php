<?php

namespace App\Http\Middleware;

use Closure;

class redirectWebSite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() )

            return $next($request);

         else


//             session()->flash('errors', trans('Site.general.signUpAndLogin') );
             session()->flash('errors', trans('Site.login.text_static_register') );

            return redirect(route('firstPageSite'));


    }
}
