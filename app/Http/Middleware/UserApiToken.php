<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Product;
use App\User;
use Closure;

class UserApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	 $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
  
	 if ( ! $user ) {   return $this->UserNotFound();  }
  
	 
        	return $next($request);
    }
       
       private  function UserNotFound(){
	    return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
       }
       
     
}
