<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Countery;
use App\User;
use Closure;

class validOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $category = Category::whereId( $request->categoryId)->first();

        if ( ! $category ) {   return $this->categoryNotFound();  }


        $city = Countery::whereId( $request->cityId)->first();

        if ( ! $city ) {   return $this->CityNotFound();  }

        
        return $next($request);
    }
    
    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }

    private  function categoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) trans('global.category_not_found')   ],200);
    }
}
