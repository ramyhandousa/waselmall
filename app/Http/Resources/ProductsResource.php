<?php

namespace App\Http\Resources;

use App\Models\Cart;
use App\Models\Color;
use App\Models\Size;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->userId){

            $user = \App\User::whereId($request->userId)->first();

            if ($user){

                $cart = Cart::getCart($user);
                $checked = collect($cart)->pluck('product_id')->toArray();
                $basketId =  collect($cart)->where('product_id', $request->productId)->first();

            }

        }
        return [
            'id' => (string)$this->id,
            'name' => $this->name,
            'desc' => $this->description,
            'price' => (string) $this->price,
            'offer' => (string)$this->is_offer,
            'discount' =>  number_format( ( $this->offer_price /$this->price   ) *100 ,0)  ,
            'priceAfter' =>  $this->offer_price   ,
            'rate' => 0 ,
            'isBasket' => $this->when($request->userId   ,
                isset( $checked) ? in_array($this->id, $checked) ? 1 : 0 : 0
                ) ,
            'basketId' => $this->when($request->userId   ,
                isset( $basketId) ? (string) $basketId->cart_id : 0
                ) ,
            'isFavorite' =>
                \App\User::Where('id',request()->userId)->first()

                    ?   count( \App\User::Where('id',request()->userId)->first()->my_favorites->where('id',$this->id)->first() ) > 0 ? 1 : 0

                    : 0
            ,
            'imageName' => $this->when( count($this->images) > 0 ,
                \App\uploadImages::whereId($this->images[0])->first()->url

            ),
            'imgesArr' => $this->when( count($this->images) > 0 , $this->getImagesWithIds($this->images)),
            'colorsArr' => $this->when( count($this->colours) > 0 , $this->getColorsWithIds($this->colours)),
            'sizeArr' => $this->when( count($this->size) > 0 , $this->getSizessWithIds($this->size)),
//
//            ) ,
        ];
    }


    private function getImagesWithIds($images){
        $image = \App\uploadImages::whereIn('id',$images)->select('id','url')->get();
        $image->map(function ($q){
            $q->imageId = (string) $q->id;
            $q->imageName = $q->url;
        });

        return $image;
    }
    private function getSizessWithIds($sizes){
        $image = Size::whereIn('id',$sizes)->select('id')->get();

        return $image;
    }
    private function getColorsWithIds($colors){
        $image =  Color::whereIn('id',$colors)->get();

        return $image;
    }
}
