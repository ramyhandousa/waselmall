<?php

namespace App\Http\Resources;

use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Resources\Json\JsonResource;

class BasketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $product = Product::whereId($this->product_id)->first();
        return [
            'basketProductId' => (string)$this->id,
            'productId' => (string)$this->product_id,
            'productName' => $product->name,
            'price' => (string)$this->price,
            'offer' => (string)$this->is_offer,
            'discount' =>  number_format( ( $product->offer_price /$this->price   ) *100 ,0)  ,
            'priceAfter' =>  $product->offer_price   ,
            'productQuantity' =>  $this->quantity ,
            'productColor' =>  Color::whereId($this->options['color_id'])->first()->name ,
            'productSize' =>  Size::whereId($this->options['size_id'])->first()->name,
            'isFavorite' =>
                \App\User::Where('id',request()->userId)->first()

                    ?   count( \App\User::Where('id',request()->userId)->first()->my_favorites->where('id',$this->product_id)->first() ) > 0 ? 1 : 0

                    : 0
            ,
            'imageName' => $this->when( count($product->images) > 0 ,
                \App\uploadImages::whereId($product->images[0])->first()->url

            ) ,
        ];
    }



}
