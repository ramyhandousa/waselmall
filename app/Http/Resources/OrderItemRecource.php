<?php

namespace App\Http\Resources;

use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\uploadImages;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::whereId($this->product_id)->first();
        return [
            'id' => (string) $this->id,
            'productName' => $product->name,
            'imageName' => uploadImages::whereId($product['images'][0])->first()->url ,
            'price' => (string) $this->price ,
            'quantity' =>(string)  $this->when($this->quantity , $this->quantity) ,
            'size' => Size::whereId($this->size_id)->first()->name,
            'color' => Color::whereId($this->color_id)->first()->name,
        ];
    }
}
