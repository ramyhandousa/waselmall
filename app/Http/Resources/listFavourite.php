<?php

namespace App\Http\Resources;

use App\Image;
use App\Models\Cart;
use Illuminate\Http\Resources\Json\JsonResource;

class listFavourite extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = \App\User::whereId($request->userId)->first();
        if ($user){
            $checked = collect(Cart::getCart($user))->pluck('product_id')->toArray();
        }

        return [
            'id' => (string)$this->id,
            'name' => $this->name,
            'desc' => $this->description,
            'price' => $this->price,
            'offer' => (string)$this->is_offer,
            'discount' =>  number_format( ( $this->offer_price /$this->price   ) *100 ,0)  ,
            'priceAfter' =>  $this->offer_price   ,
            'rate' => 0 ,
//            'isBasket' => in_array($this->id, $checked) ? 1 : 0 ,
//            ,
            'isBasket' => $this->when($user   ,
                isset( $checked) ? in_array($this->id, $checked) ? 1 : 0 : 0
            ) ,
            'imageName' => $this->when( count($this->images) > 0 ,
                \App\uploadImages::whereId($this->images[0])->first()->url

            ) ,
        ];
    }
}
