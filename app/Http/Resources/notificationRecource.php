<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use Illuminate\Http\Resources\Json\JsonResource;

class notificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $havePreviousConversation = Conversation::where('ad_id',$this->ads_id)->whereHas('users', function ($q)  {
            $q->whereId($this->sender_id);
        })->whereHas('users', function ($q)  {
            $q->whereId($this->user_id);
        })->first();

        return [
            'id' => $this->id,
            'title' => $this->when($this->title , $this->title) ,
            'body' => $this->when($this->body , $this->body) ,
            'is_read' =>  $this->is_read ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'conversation_id'     =>
                $this->when(($this->type == 4) ||($this->type == 5)  ,
                // Check if has Conversation before or not
                                    $havePreviousConversation ? $havePreviousConversation->id : 0
                            ),
        ];
    }
}
