<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => (string) $this->id,
            'orderDate' => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'paymentType' => $this->when($this->payment_type , $this->payment_type) ,
            'detailsCount' => (string) Order::getTotalItems($this->id) ,
            'total' =>(string)  $this->when($this->total_price , $this->total_price) ,
            'details' => OrderItemRecource::collection($this->whenLoaded('items')) ,
        ];
    }
}
