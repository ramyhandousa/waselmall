<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class City extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {
        $data  = [
            'id'       =>  $this->id,
            'name'     => $this->name,
            'parent'   => $this->when($this->parent, new Parentcity($this->parent)) ,
        ];

        return $data;
    }
}
