<?php

namespace App\Http\Resources;

use App\Models\Ad;
use App\Models\Conversation;
use App\Models\Image;
use Hamcrest\Thingy;
use Illuminate\Http\Resources\Json\JsonResource;

use Illuminate\Http\Request;
class Ads extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {

        if (request()->headers->get('apiToken')){

            $conversation = Conversation::where('ad_id',$this->id)->whereHas('users', function ($q)  {
                $q->whereId(\App\User::where( 'api_token' , request()->headers->get('apiToken') )->first()->id);
            })->first();

        }

        return [

            'id' => $this->id,
            $this->mergeWhen(request()->headers->get('apiToken'),
            [

                'conversation_id' => request()->headers->get('apiToken') ?
                                        $conversation ? $conversation->id : 0
                                        : 0
            ]),

            'name' => $this->when($this->name , $this->name) ,
            'description' => $this->when($this->description , $this->description) ,
            'phone' => $this->when($this->phone , $this->phone) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
            'images' => $this->when($this->images , $this->getImagesWithIds($this->images)) ,
            'can_comment' =>  $this->can_comment   ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'views' => $this->when($this->views , count($this->views)) ,
            'category' =>  $this->when($this->category_id , new City($this->category)),
            'city' =>  $this->when($this->city_id , new City($this->city)),

            'user' => new User($this->whenLoaded('user')),
            $this->mergeWhen(
                // Api Token
                request()->headers->get('apiToken')||
                // Get User By Id
                        request()->id == $this->user_id ,
//
                // Get ADs By Id
//                       || request()->query('id') == $this->id,
            [


                'favorite' =>

                 \App\User::where('api_token',request()->headers->get('apiToken'))->orWhere('id',request()->id)->first()

                     ?       count(\App\User::where('api_token',request()->headers->get('apiToken'))
                                    ->orWhere('id',request()->id)->first()->my_favorites
                                    ->where('id',$this->id)->first()
                                    ) > 0 ? 1 : 0

                     : 0

                ,
                'is_follower' =>

                 \App\User::where('api_token',request()->headers->get('apiToken'))->orWhere('id',request()->id)->first()

                     ?       count(\App\User::where('api_token',request()->headers->get('apiToken'))
                                    ->orWhere('id',request()->id)->first()->followers
                                    ->where('id',$this->user_id)->first()
                                    ) > 0 ? 1 : 0

                     : 0

                ,

            ]),

            // This favorite for Skip User
            $this->mergeWhen(!request()->headers->get('apiToken')||!request()->id || !request()->query('id'),
            [
                'favorite' => 0,
                'is_follower' => 0,
            ]),

            $this->mergeWhen($this->whenLoaded('comments'),
            [
                'comments' => Comments::collection($this->whenLoaded('comments') )
            ]),

            // want parent id  == 0 only and get children []
            $this->mergeWhen($this->whenLoaded('mainComments'),
            [
                'comments' => Comments::collection($this->whenLoaded('mainComments') )
            ]),

        ];
    }

    private function getImagesWithIds($images){
        $image = Image::whereIn('id',$images)->select('id','url')->get();

        return $image;
    }

}
