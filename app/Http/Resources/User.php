<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'phone' => $this->phone,
            'email' => $this->when($this->email , $this->email) ,
            'date_of_birth' => $this->when($this->data_of_birth , $this->data_of_birth) ,
            'gender' => (string) $this->when($this->gender , $this->gender) ,
            'image' => $this->when($this->image , $this->image) ,
            'address' => $this->when($this->address , $this->address) ,
            'company_name' => $this->when($this->company_name , $this->company_name) ,
            'company_phone' => $this->when($this->company_phone , $this->company_phone) ,
            'company_fax' => $this->when($this->company_fax , $this->company_fax) ,
            'zip_code' => $this->when($this->zip_code , $this->zip_code) ,
            'code' => $this->when($this->code , $this->code['action_code'] ) ,
            'active' => (string)$this->is_active,
            'is_suspend' => $this->when($this->is_suspend , $this->is_suspend) ,
            'message' => $this->when($this->message, $this->message) ,
            'city' =>  $this->when($this->city_id , new City($this->city)),
            'brand' =>  $this->when($this->brand_id , new City($this->brand)),
        ];
    }

}
