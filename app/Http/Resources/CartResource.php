<?php

namespace App\Http\Resources;

use App\Models\Cart;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'userId' => (string)Cart::whereId($this->cart_id)->first()->user_id,
            'productId' => (string)$this->product_id,
            'productQuantity' => $this->quantity,
            'productColor' => $this->options['color_id'],
            'productSize' => $this->options['size_id'],
        ];
    }
}
