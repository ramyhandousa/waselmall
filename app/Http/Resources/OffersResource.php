<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OffersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'name' => $this->name,
            'desc' => $this->description,
            'price' => $this->price,
            'offer' => (string)$this->is_offer,
            'discount' =>  number_format( ( $this->offer_price /$this->price   ) *100 ,0)  ,
            'priceAfter' =>  $this->offer_price   ,
            'imageName' => $this->when( count($this->images) > 0 ,
                \App\uploadImages::whereId($this->images[0])->first()->url

            ) ,
        ];
    }
}
