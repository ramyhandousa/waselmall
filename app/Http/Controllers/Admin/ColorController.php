<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;

class ColorController extends Controller
{
    public function index()
    {

        $brands = Color::latest()->get();

        $pageName = 'إدارة الألون';
        return view('admin.colors.index',compact('brands','pageName'));
    }

    public function create()
    {

        $pageName = 'إسم اللون ';
        return view('admin.colors.create',compact('pageName'));
    }


    public function store(Request $request)
    {

        $brand  = new Color();
        $brand->hash_code = $request->hash_code;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;


        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الألوان']),
            "url" => route('colors.index'),

        ]);
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $brand = Color::findOrFail($id);
        $pageName = 'اسم  الوان ';

        return view('admin.colors.edit',compact('brand','pageName'));
    }

    public function update(Request $request, $id)
    {
        $brand = Color::findOrFail($id);
        $brand->hash_code = $request->hash_code;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الألوان']),
            "url" => route('colors.index'),

        ]);
    }

    public function destroy($id)
    {
        //
    }



    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = Color::findOrFail($request->id);


        $products =  Product::pluck('colours')->collapse();

//        if ($model->products->count() > 0) {
//            return response()->json([
//                'status' => false,
//                'message' => 'عفواً, لا يمكنك حذف ً   اللون لوجود منتجات بها'
//            ]);
//        }
//
////        if ($model->delete()) {
        if ($model) {
//          $model->deleteTranslations();
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

}
