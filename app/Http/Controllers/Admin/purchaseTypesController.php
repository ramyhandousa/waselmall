<?php

namespace App\Http\Controllers\Admin;

use App\Models\PurchaseType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;
class purchaseTypesController extends Controller
{
    public function index()
    {

        $brands = PurchaseType::latest()->get();

        $pageName = 'إدارة الخصومات او العمليات علي المنتجات';

        return view('admin.purchaseTypes.index',compact('brands','pageName'));

    }

    public function create()
    {

        $pageName = 'إسم الخصم ';
        return view('admin.purchaseTypes.create',compact('pageName'));
    }


    public function store(Request $request)
    {

        $brand  = new PurchaseType();
        $brand->price = $request->price;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;


        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الخصومات']),
            "url" => route('purchaseTypes.index'),

        ]);
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $brand = PurchaseType::findOrFail($id);
        $pageName = 'اسم  الخصم ';

        return view('admin.purchaseTypes.edit',compact('brand','pageName'));
    }

    public function update(Request $request, $id)
    {
        $brand = PurchaseType::findOrFail($id);
        $brand->price = $request->price;
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الخصومات']),
            "url" => route('purchaseTypes.index'),

        ]);
    }

    public function destroy($id)
    {
        //
    }



    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = PurchaseType::findOrFail($request->id);


        if ($model->products->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => 'عفواً, لا يمكنك حذف ً لوجود خصومات حالية عليها'
            ]);
        }

        if ($model->delete()) {
            $model->deleteTranslations();
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }
}
