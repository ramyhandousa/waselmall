<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\City;
use App\Models\Image;
use App\User;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{


    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/users/';
    }
    
    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $quary = User::whereDoesntHave('roles')->whereDoesntHave('abilities');

        if ($request->type == 'clients'){
            $quary->where('defined','user');
        }else{
            $quary->where('defined','company');
        }

        $users =  $quary->latest()->get();

        return view('admin.users.index', compact('users'));

    }


  
    public function create()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $brands = Brand::where('is_suspend',0)->get();
        $cities = City::where('is_suspend',0)->whereParentId(0)->get();

        return view('admin.users.create', compact('brands','cities'));
    }

  
    public function store(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        //Get input ...
        $post_data = [
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => $request->password,
            'confirm_password' => $request->password_confirmation,
            'image' => $request->image,
        ];

        //set Rules .....
        $valRules = [
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
        ];

        //Declare Validation message
        $valMessages = [
            'phone.required' => 'رقم الهاتف مطلوب',
            'phone.unique' => 'هذا الرقم مستخدم من قبل',
            'email.required' => 'البريد الإلكتروني مطلوب',
            'email.unique' => 'هذا البريد مستخدم من قبل',
            'password.required' => 'كلمة المرور مطلوبة',
            'confirm_password.same' => 'كلمة المرور غير متطابقة',
        ];

        //validate inputs ......
        $valResult = Validator::make($post_data, $valRules, $valMessages);
        if ($valResult->passes()) {
            $user = new User;
            $user->defined = 'company';
            $user->gender = $request->gender;
            $user->brand_id = $request->brand;
            $user->city_id = $request->city;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = $request->password;
            $user->address = $request->address;
            $user->is_active = 1;

            if ($request->hasFile('image')):
                $user->image = $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path, 1280, 583);
            endif;

            $user->save();

            session()->flash('success', 'لقد تم إضافة شركة بنجاح.');
            return redirect()->route('users.index');
        } else {
            // Grab Messages From Validator
            $valErrors = $valResult->messages();
            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);
        }
        

    }
 
    public function show($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $user = User::findOrFail($id);



        return view('admin.users.show', compact('user'));
    }
    


    public function edit($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.users.edit');
    }

   
    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
    }
    
    public function destroy($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
       
    }
    
    
    public function accpetedUser(Request $request){
        $user = User::findOrFail($request->id);
        
        if ($user){
            $user->update(['is_accepted' => 1]);
            return response()->json( [
                'status' => true ,
            ] , 200 );
            
        }else{
            
            return response()->json( [
                'status' => false
            ] , 200 );
            
        }
        
    }


    


}
