<?php

namespace App\Http\Controllers\Site\Auth;

use App\Mail\ResetEmail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ResetController extends Controller
{
    public function resetEmail(){

        return view('Site.Auth.email');
    }


    public function sendMail(Request $request){

       $user =  User::whereEmail($request->email)->first();

       if (!$user){
           session()->flash('errors', 'تأكد من إيميلك الشخصي  من فضلك .');
           return back();
       }

       $user->update(['token_reset' => str_random(60)]);

        Mail::to($user->email)->send(new ResetEmail($user));

    }


    public function updatePassword(){

        return view('Site.Auth.reset');

    }


    public function updatePass(Request $request){

        $data =  $request->all();

        $user  = User::where('token_reset',$data['token_reset'])->first();
        if (!$user){
            session()->flash('errors', 'تم تغير البيانات سابقا....');
            return back();
        }

        $user->update(['password' => $data['password'],'token_reset' => ' ']);


        session()->flash('success', 'تم تغير الباسورد بنجاح.');

        return view('Site.Auth.login');

    }



}
