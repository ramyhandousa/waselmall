<?php

namespace App\Http\Controllers\Site\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function index(){

        return view('Site.Auth.login');

    }


    public function login(Request $request){


        if (Auth::attempt(["email" => $request->email, 'password' => $request->password])) {

             session()->flash('success', 'تم التسجيل  الدخول بنجاح.');

            return redirect()->route('firstPageSite');
        }

        session()->flash('errors',   __('trans.email_or_password_error')  );

        return redirect()->back()->withInput();
    }

    public function logOut(Request $request){

        Auth::guard()->logout();

        $request->session()->flush();
        $request->session()->regenerate();
        session()->flash('success', 'تم التسجيل  الخروج بنجاح.');

        return redirect()->route('firstPageSite');
    }
}
