<?php

namespace App\Http\Controllers\Site\Auth;

use App\Models\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{

    public function index(){

        $countery = City::whereParentId(0)->get();
//        return $countery;
        return view('Site.Auth.register' , compact('countery'));
    }
    public function register(Request $request){

//        return $request->all();

        $data_of_birth = $request->year . '-' . $request->month . '-' . $request->day;

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => $request->password,
            'address' => $request->address,
            'city_id' => $request->city,
            'gender' => $request->gender,
            'data_of_birth' => $data_of_birth,
            'company_name' => $request->company,
            'company_phone' => $request->telephone,
            'company_fax' => $request->fax,
            'zip_code' => $request->zip,
        ]);

        \Auth::loginUsingId($user->id);

        session()->flash('success', 'تم التسجيل بنجاح.');

        return redirect()->route('firstPageSite');

    }
}
