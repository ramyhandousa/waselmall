<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\uploadImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    public function __construct()
    {
        app()->setLocale(config('app.locale'));
    }


    public function index(){

        $brands = Brand::where('is_suspend',0)->get();

        $colors = Color::all();
        $sizes = Size::all();


        return view('Site.products.search',compact('brands','colors','sizes'));

    }

    public function search(Request $request){

        $brands = Brand::where('is_suspend',0)->get();

        $colors = Color::all();
        $sizes = Size::all();

        $keyword =  $request->name  ;
        $brand =  $request->brand  ;
        $category =  $request->category  ;
        $color =  $request->color  ;
        $size =  $request->size  ;
        $priceFrom =  $request->priceFrom  ;
        $priceTo =  $request->priceTo  ;


        $query = Product::where('is_suspend', 0)->select('id','price','category_id','images','is_offer','offer_price')->with('category');

        if ($category){  $query->where('category_id','LIKE',$category); }

        if ($brand){  $query->addSelect('brand_id')->where('brand_id','LIKE',$brand); }

        if ($size){  $query->addSelect('size')->where('size', 'like', "%\"{$size}\"%"); }

        if ($color){  $query->addSelect('colours')->where('colours', 'like', "%\"{$color}\"%"); }

        if ($priceFrom || $priceTo){  $query->whereBetween('price', [$priceFrom, $priceTo]); }


        $data =  $query->get();

        $data->map(function ($q) {
            $q->images = $q->images ?$this->getImagesWithIds($q->images) : null;
        });



        if ($keyword && $keyword != ''){

            $data =  $data->filter(function ($item) use ($keyword) {
                return data_get($item, 'name') == $keyword;
            })->values();


            return view('Site.products.search',compact('data','brands','colors','sizes'));

        }





        return view('Site.products.search',compact('data','brands','colors','sizes'));

    }


    private function getImagesWithIds($images){
        $image = uploadImages::whereIn('id',$images)->get(['url']);

        return $image;
    }





}
