<?php

namespace App\Http\Controllers\Site;

use App\Models\City;
use App\Models\Product;
use App\Models\Size;
use App\Profile;
use App\uploadImages;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \Auth::user();

        return view('Site.user.profile',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user();

        return view('Site.user.editProfile',compact('user'));
    }

    public function editAddress(){


        $user = \Auth::user();
        $countery = City::whereParentId(0)->get();

        return view('Site.user.editAddress',compact('user','countery'));
    }

    public function addAddress(Request $request){

         $id = \Auth::id();
         $user = User::findOrFail($id);

         $profile = new  Profile();
         $profile->user_id = $user->id;
         $profile->address = $request->address;
         $profile->city_id = $request->city;
         $profile->company_name = $request->company;
         $profile->company_phone = $request->telephone;
         $profile->company_fax = $request->fax;
         $profile->save();

         session()->flash('success',  trans('Site.profile.add_account_book_success') );
        return redirect()->back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $postData = $this->postData($request);

        // Declare Validation Rules.
        $valRules = $this->valRules($id);

        // Declare Validation Messages
        $valMessages = $this->valMessages();

        // Validate Input
        $valResult = Validator::make($postData, $valRules, $valMessages);

        // Check Validate
        if ($valResult->passes()) {


            if ( $request->currentPass && ! Hash::check($request->currentPass,  \Auth::user()->password) ) {

                session()->flash('errors',   trans('global.old_password_is_incorrect')  );
                return redirect()->back();
            }

            $user = User::findOrFail($id);

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->gender = $request->gender ?: $user->gender;

            if ($request->year && $request->month && $request->day  ){

                $data_of_birth = $request->year . '-' . $request->month . '-' . $request->day;

                $user->data_of_birth = $data_of_birth;
            }

            if ($request->newPass) {

                $user->password = $request->newPass;
            }
            $user->save();


            if ($request->newPass) {

                session()->flash('success',  trans('Site.profile.edit_personal_data_and_pass') );

            }else{

                session()->flash('success', trans('global.profile_edit_success') );
            }

            return redirect()->back();

        } else {

            $valErrors = $valResult->messages();

            // Error, Redirect To User Edit

            return redirect()->back()->withInput()
                ->withErrors($valErrors);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function  listFavourite() {

        $user = \Auth::user();

        $products =  $user->my_favorites ;


        $products->map(function ($q) {
            $q->images = $q->images ?$this->getImagesWithIds($q->images) : null;
            $q->size = $q->size ?$this->getSizes($q->size) : null;
        });


        return view('Site.user.listFavourites',compact('products'));
    }



    public  function makeFavourite(Request $request) {

        $user = \Auth::user();

        if (!\Auth::check()){

            return response()->json([
                'status' => 400,
                'error' => (array)  __('Site.login.text_static_register')
            ], 200);
        }

        if(!$user->my_favorites()->where('product_id', $request->productId)->first()){

            $user->my_favorites()->attach($request->productId);

            return response()->json([
                'status' => 200,
                'message' => __('global.user_add_favorite')
            ], 200);

        }else{

            $user->my_favorites()->detach($request->productId);

            return response()->json([
                'status' => 201,
                'message' => __('global.user_remove_favorite')
            ], 200);
        }


    }


    private function postData($request)
    {
        return [
            'email' => $request->email,
            'phone' => $request->phone,
        ];
    }

    /**
     * @return array
     */
    private function valRules($id)
    {
        return [
            'email' => 'required|email|unique:users,email,' . $id,
            'phone' => 'required|unique:users,phone,' . $id,
        ];
    }

    /**
     * @return array
     */
    private function valMessages()
    {
        return [
            'email.required' => trans('global.field_required'),
            'email.unique' => trans('global.unique_email'),
            'phone.required' => trans('global.field_required'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }


    private function getImagesWithIds($images){
        $image = uploadImages::whereIn('id',$images)->get(['url']);

        return $image;
    }

    private function getSizes($sizes){
        $sizes = Size::whereIn('id',$sizes)->select('id')->get();

        return $sizes;
    }

}
