<?php

namespace App\Http\Controllers\Site;

use App\Models\Setting;
use App\Models\Support;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{


    public function aboutUs(){

        $setting = Setting::all();

        $vision = $setting->where('key','our_vision_' . config('app.locale'))->first() ;
        $mission = $setting->where('key','our_mission_' . config('app.locale'))->first() ;
        $rate_us = $setting->where('key','rate_us_' . config('app.locale'))->first() ;

        $dataSetting = [
            'vision' => $vision ? $vision->body : 0,
            'mission' => $mission ? $mission->body : 0,
            'rate_us' => $rate_us ? $rate_us->body : 0,
        ];


        return view('Site.setting.about',compact('dataSetting'));

    }

    public function Privacy(){

        $privacy = Setting::where('key','terms_user_' . config('app.locale'))->first() ;

        $dataSetting = [
            'privacy' => $privacy ? $privacy->body : ' ',
        ];


        return view('Site.setting.privacy',compact('dataSetting'));

    }

    public function contactUsIndex(){

        $setting = Setting::all();

        $phone = $setting->where('key','contactus_phone')->first() ;
        $email = $setting->where('key','contactus_email')->first() ;

        $dataSetting = [
            'phone' => $phone ? $phone->body : trans('Site.contact.phone'),
            'email' => $email ? $email->body : trans('Site.contact.title'),
        ];

        return view('Site.setting.contact_us', compact('dataSetting') );

    }

    public function contactUs(Request $request){

        $data =  $request->all();

        $support = new Support();
        $support->type_id = 2;
        $support->name = $data['name'];
        $support->address = $data['address'];
        $support->message = $data['message'];
        $support->save();

         session()->flash('success', trans('Site.contact.message') );

        return redirect()->back();

    }




}
