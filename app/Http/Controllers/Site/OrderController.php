<?php

namespace App\Http\Controllers\Site;

use App\Models\Cart;
use App\Models\City;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{


    public function index(){

       $setting = Setting::all();
        $countery = City::whereParentId(0)->whereHas('children')->get();
       $services_purchases = $setting->where('key','services_purchases')->first() ;
       $payment_cash = $setting->where('key','payment_cash')->first() ;
       $taxs = $setting->where('key','taxs')->first() ;

       $dataSetting = [
         'service' => $services_purchases ? $services_purchases->body : 0,
         'payment_cash' => $payment_cash ? $payment_cash->body : 0,
         'taxs' => $taxs ? $taxs->body : 0,
       ];


        $user = Auth::user();

        $cart = Cart::getCart(Auth::user());

        $totalItemsPrice = 0;

        if ($cart){

            foreach ($cart as  $value) {

                $totalItemsPrice += ($value['price'] * (integer)$value['quantity']);

            }
        }


        return view('Site.orders.index',compact('user','countery','dataSetting','totalItemsPrice'));

    }


    public function storeOrder(Request $request){

        $data = $request->all();
        $data['phoneOrder'] =  $request->country_code . $request->phone;
        $data['cart'] = Cart::getCart(\Auth::user());

        return $data;

    }




}
