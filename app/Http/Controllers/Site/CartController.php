<?php

namespace App\Http\Controllers\Site;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Color;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $setting = Setting::all();

        $services_purchases = $setting->where('key','services_purchases')->first() ;
        $payment_cash = $setting->where('key','payment_cash')->first() ;
        $taxs = $setting->where('key','taxs')->first() ;

        $dataSetting = [
            'service' => $services_purchases ? $services_purchases->body : 0,
            'payment_cash' => $payment_cash ? $payment_cash->body : 0,
            'taxs' => $taxs ? $taxs->body : 0,
        ];

        $cart = Cart::getCart(Auth::user());

        $totalItemsPrice = 0;

        if ($cart){

            foreach ($cart as  $value) {

                $totalItemsPrice += ($value['price'] * (integer)$value['quantity']);

            }
        }

        return view('Site.products.basket',compact('dataSetting','totalItemsPrice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if (!\Auth::check()){

            session()->flash('errors',   __('Site.login.text_static_register')  );

            return redirect()->back();
        }

        if ($request->productId) {

            $additional = [
                'size_id' =>  $request->size ?: 1 ,
                'color_id' =>  $request->color ?: 1
            ];

            Cart::addToCart(\Auth::user(),$request->productId ,$request->quantity,$request->price, $additional);

            session()->flash('success',   __('site.cart.add_to_cat_name')  );

            return redirect()->back();

        }else{

            session()->flash('errors',   __('global.product_not_found')  );

            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $setting = Setting::all();

        $services_purchases = $setting->where('key','services_purchases')->first() ;
        $payment_cash = $setting->where('key','payment_cash')->first() ;
        $taxs = $setting->where('key','taxs')->first() ;

        $dataSetting = [
            'service' => $services_purchases ? $services_purchases->body : 0,
            'payment_cash' => $payment_cash ? $payment_cash->body : 0,
            'taxs' => $taxs ? $taxs->body : 0,
        ];


        $cart = Cart::getCart(Auth::user());

        $totalItemsPrice = 0;

        if ($cart){

            foreach ($cart as  $value) {

                $totalItemsPrice += ($value['price'] * (integer)$value['quantity']);

            }
        }

      return view('Site.products.basket',compact('dataSetting','totalItemsPrice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $cart = Cart::findOrfail($id);

        $sizes= Size::all();
        $colors = Color::all();

        $totalItemsPrice = 0;

        foreach ($cart->items as  $value) {

            $totalItemsPrice += ($value['price'] * (integer)$value['quantity']);

        }

         return view('Site.products.editBasket',compact('sizes','colors','cart','totalItemsPrice','idsOfItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


//        $product = Product::whereId($request->product_id)->first();
//
//        $productQuantity = $product->quantity;
//
//        if ($request->quantity > $productQuantity){
//
//            return response()->json([
//                'status' => 400,
//                'message' => __('Site.products.max_quantity',['attribute' => $productQuantity])
//            ], 200);
//
//        }
//
//        $additional = [
//            'size_id' =>  $request->size  ,
//            'color_id' =>  $request->color
//        ];


        $inputs = $request->all();

        for ($i = 0; $i < $request->cartCount; $i++){
                if(!$inputs['quantity'][$i])
                    continue;

                $cart = CartItem::whereId($inputs['cartIds'][$i])->first();
                $product = Product::whereId($cart->product_id)->first();

                $productQy  = $product->quantity;
                $cart->quantity = $inputs['quantity'][$i];

                if ($inputs['quantity'][$i] > $productQy   ){
                    session()->flash('errors', __('Site.cart.Q_max',['name' => $product->name]) );
                    return back();
                }

//              $product->update(['quantity' => ($productQy - $cart->quantity) ]);



                $additional = [
                    'size_id' =>  $inputs['sizes'][$i],
                    'color_id' =>  $inputs['colors'][$i]
                ];
                $cart->options = $additional;

                $cart->save();
        }

        session()->flash('success', __('Site.cart.edit_to_cart_success'));
        return back();

//        Cart::addToCart(\Auth::user(),$product->id ,$request->quantity,$product->price, $additional);
//
//        return response()->json([
//            'status' => 200,
//            'message' => __('Site.cart.edit_to_cart_success')
//        ], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function simpleAdd(){
        //        $user = \Auth::user();
//
//        $cart= new Cart();
//        $cart->user_id = $user->id;
//        $cart->save();
//
//        $cartItems = new CartItem();
//        $cartItems->cart_id = $cart->id;
//        $cartItems->product_id = $request->productId;
//        $cartItems->size_id  = $request->size;
//        $cartItems->color_id = $request->color;
//        $cartItems->quantity = $request->quantity;
//        $cartItems->price  = $request->priceProduct;
//        $cartItems->save();


    }


    public function delete(Request $request){


        $model = CartItem::findOrFail($request->id);


//        if ($model->delete()) {
        if ($model) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }
}
