<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\DealOfDay;
use App\Models\Product;
use App\Models\Size;
use App\uploadImages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index(Request $request){




        $query = Product::where('is_suspend', 0)->whereHas('brand_filter');

        // ============== This is [ id ] means id of category
        if ($request->id){
            $query->whereCategoryId($request->id);

        }

        if ($request->brandId){

            $query->whereBrandId($request->brandId);
        }

        $products =  $query->with('category','brand_filter','currency','offer_day')->latest()->get();

        $products->map(function ($q) {
            $q->images = $q->images ?$this->getImagesWithIds($q->images) : null;
        });


        if ($request->id){

            $brandsProducts = Product::where('is_suspend', 0)
                                ->whereHas('brand_filter')
                                ->whereCategoryId($request->id)
                                ->with('brand_filter')
                                ->get()->pluck('brand_filter')
                                ->unique()->flatten();

            $category =  $products->filter(function ($item) use ($request) {
                return data_get($item, 'category_id') == $request->id;

            })->pluck('category')->unique()->flatten()->first();

        }



        return view('Site.products.index',compact('products','brandsProducts','category'));

    }

    public function show(Request $request){

        $product = Product::with('product_purchases.purchase','offer_day')->findOrFail($request->id);

        if (\Auth::check()){
            $product['is_favorite'] =  \Auth::user()->my_favorites->where('id',$product->id)->first() ? 1 :0;
        }else{
            $product['is_favorite'] =  0;
        }


        if (!empty($product->images)){
            $product['images'] =  $product['images'] ?$this->getImagesWithIds($product->images) : null;
        }
        if (!empty($product->size)){
            $product['size'] =  $product['size'] ?$this->getSizes($product->size) : null;
        }

        if (!empty($product->colours)){
            $product['colours'] =  $product['colours'] ?$this->getColors($product->colours) : null;
        }

        $product_purchases =  $product['product_purchases']->pluck('purchase');


        $productsSizes = $product['size'];
        $productsColors = $product['colours'];


        $filterDeals = function ($q){
            $q->select('id','images','quantity','price','category_id','currency_id','is_offer','offer_price');
        };
        $dealsOfDay = DealOfDay::whereHas('product',function ($q){
            $q->where('is_suspend', 0);
        })->with(['product' => $filterDeals])->where('end_data','>=',Carbon::today())->get();

        $dealsOfDay->map(function ($q) {
            $q->product->images = $q->product->images ?$this->getImagesWithIds($q->product->images) : null;
        });



        return view('Site.products.show',compact('product','product_purchases','productsColors','productsSizes','dealsOfDay'));
    }



    private function getImagesWithIds($images){
        $image = uploadImages::whereIn('id',$images)->get(['url']);

        return $image;
    }

    private function getSizes($sizes){
        $sizes = Size::whereIn('id',$sizes)->select('id')->get();

        return $sizes;
    }

    private function getColors($colors){
        $colors = Color::whereIn('id',$colors)->select('id','hash_code')->get();

        return $colors;
    }


    private function filterbrandsProducts(){

//            $brandsProducts =  $products->filter(function ($item) use ($request) {
//                return data_get($item, 'category_id') == $request->id;
//
//            })->pluck(['brand_filter'])->unique()->flatten();

    }




}
