<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\DealOfDay;
use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\PurchaseType;
use App\uploadImages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends Controller
{
    public function index(){

        $purchase_types = PurchaseType::all();

        $filterDeals = function ($q){
          $q->select('id','images','quantity','price','category_id','currency_id','is_offer','offer_price');
        };
        $dealsOfDay = DealOfDay::whereHas('product',function ($q){
            $q->where('is_suspend', 0);
        })->where('end_data','>=',Carbon::today())->with(['product' => $filterDeals])->get();

        $dealsOfDay->map(function ($q) {
            $q->product->images = $q->product->images ?$this->getImagesWithIds($q->product->images) : null;
        });

        $brands = Brand::where('is_suspend',0)->get();

        return view('Site.index',compact('purchase_types','dealsOfDay','brands'));
    }


    public function getIndex(Request $request){

        $data = ProductPurchase::where('purchase_type_id',$request->id)->with('product_name')->latest()->get();

        $products = collect($data)->pluck('product_name');
        return $products;

        return $data;
    }


    private function getImagesWithIds($images){
        $image = uploadImages::whereIn('id',$images)->get(['url']);

        return $image;
    }
}
