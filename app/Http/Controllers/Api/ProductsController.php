<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\brandRecource;
use App\Http\Resources\listFavourite;
use App\Http\Resources\OffersResource;
use App\Http\Resources\ProductsResource;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }


    public function listProducts(Request $request){

        $products = Product::whereId($request->productId)->where('is_suspend',0)->first();


        if (!$products){
            return $this->ProductNotFound();
        }


//        $data = ProductsResource::collection($products);
        $data = new  ProductsResource($products);

        return response()->json([
            'status' => 200,
            'message' => "success",
            'data' => $data
        ], 200);

    }
    public function listProductsOfCategory(Request $request){

        $products = Product::whereCategoryId($request->categoryId)->where('is_suspend',0)->get();


        if (!$products){
            return $this->ProductNotFound();
        }


        $data = listFavourite::collection($products);
//        $data = new  ProductsResource($products);

        return response()->json([
            'status' => 200,
            'message' => "success",
            'data' => $data
        ], 200);

    }



    public function brands(){

        $brands =  Brand::where('is_suspend',0)->get() ;

        $data = brandRecource::collection($brands);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function offers(Request $request){

        $offers = Product::where('is_suspend',0)->where('is_offer',1)->get();


        $data = OffersResource::collection($offers);


        return response()->json([
            'status' => 200,
            'message' => "success",
            'data' => $data
        ], 200);

    }



    private  function ProductNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'Product Not found'   ],200);
    }


}
