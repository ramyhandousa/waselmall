<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\bank;
use App\Http\Resources\brandRecource;
use App\Libraries\InsertNotification;
use App\Models\BankTransfer;
use App\Models\Brand;
use App\Models\Device;
use App\Models\Faq;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Support;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\New_;
use UploadImage;

class SettingController extends Controller
{
        public $public_path;
       public $headerApiToken;
       public $language;
        public $notify;
       
       public function __construct( InsertNotification $notification)
       {
            $this->language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
            app()->setLocale($this->language);

            // api token from header
            $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

            $this->notify = $notification;

           $this->public_path = 'files/transfers';

       }
       
   public function aboutUs(){

           $data = [
               'about' =>  Setting::whereKey('about_us_' . $this->language)->first() ? Setting::whereKey('about_us_' . $this->language)->first()->body : ''  ,
               'FaceBook' =>   Setting::whereKey('contactus_facebook')->first() ?  Setting::whereKey('contactus_facebook')->first()->body : '' ,
               'twitter' =>   Setting::whereKey('contactus_twitter')->first() ?  Setting::whereKey('contactus_twitter')->first()->body : '' ,
               'insta' =>   Setting::whereKey('contactus_instagram')->first()   ? Setting::whereKey('contactus_instagram')->first()->body : '' ,
               'google' =>  Setting::whereKey('contactus_google')->first() ? Setting::whereKey('contactus_google')->first()->body : '' ,
               'youtube' =>   Setting::whereKey('contactus_youtube')->first() ?  Setting::whereKey('contactus_youtube')->first()->body : '' ,
           ];
   
        return response()->json( [
                'status' => 200 ,
                'data' => $data ,
        ] , 200 );
   }
   


   public function terms(){

       $terms =  Setting::whereKey('terms_user_' . $this->language )->first() ;

        return response()->json( [
                'status' => 200 ,
                'data' => $terms ? (array) $terms->body : (array) ' ' ,
        ] , 200 );
   }





    public function contactUs(Request $request){
        // check for user
//        $user = User::where( 'api_token' , $this->headerApiToken )->first();
//        if ( ! $user ) {   return $this->UserNotFound();  }

        $support = new Support();
//        $support->user_id = $user->id;
        $support->type_id =  1;
        $support->name = $request->name;
        $support->email = $request->email;
        $support->subject = $request->subject;
        $support->message = $request->message;
        $support->save();

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.message_was_sent_successfully'),
        ] , 200 );

    }

    public function bankTransfer(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        $bank = new BankTransfer();
        $bank->user_id =  $user->id;
        $bank->userName =  $request->userName;
        $bank->userAccount =  $request->userAccount;
        $bank->iban =  $request->iban;
        $bank->money =  $request->money;
        $bank->bank =  $request->bank;
        if ( $request->hasFile( 'image' ) ):
            $bank->image =  $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage( $request , 'image' , $this->public_path );
        endif;
        $bank->save();
        return response()->json( [
            'status' => 200 ,
            'message' => 'تمت العملية بنجاح',
        ] , 200 );
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }
   
}
