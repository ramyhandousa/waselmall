<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Models\City;
use App\Models\Countery;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage;
       public $headerApiToken;
       
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles';
	    
	    // default Image upload in profile user
	    $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/default.png';
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
       }
       
    public function register(resgister $request){
     
	    $action_code = substr( rand(), 0, 4);

	   $city  = City::whereId($request->city)->first();

	   if (!$city){
	       return $this->CityNotFound();
       }

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone =$request->phone;
        $user->email =$request->email;
        $user->city_id = $request->city;
        $user->password = $request->password;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->data_of_birth = $request->date_of_birth;
        $user->company_name = $request->company_name;
        $user->company_phone = $request->company_phone;
        $user->company_fax = $request->company_fax;
        $user->zip_code = $request->zip_code;
        $user->is_active = 0;
        $user->image = $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $this->defaultImage;
        $user->save();
        $this->createVerfiy($request , $user , $action_code);

//	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);
        $data = new \App\Http\Resources\User($user);
        return response()->json( [
            'status' => 200 ,
            'message' => "success",
            'data' => $data
        ] , 200 );
    }

   private function createVerfiy($request , $user , $action_code){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id = $user->id;
       $verifyPhone->phone =$request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }
  
}
