<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Http\Resources\Comments;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Ad;
use App\Models\Comment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{

    public $headerApiToken;

    public $push;
    public $notify;
    public function __construct(PushNotification $push,InsertNotification $notification  )
    {
        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
        $this->push = $push;

        $this->notify = $notification;
    }

    public function ListCommentBelongToAds(Request $request){


        $advertisement = Ad::whereId($request->advertisementId)->first();

        if (!$advertisement){ return $this->advertisementNotFound(); }

        $data = Comments::collection(
                                Comment::whereParentId(0)->where('ad_id',$request->advertisementId)->with('children')->get()
                                );


        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }

    public function makeComment(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $advertisement = Ad::whereId($request->advertisementId)->with('user.devices','comments')->first();
//        $deviceUser = $advertisement['user']->pluck('device');
        // Person who get Notify Of Price
        $sender =  $advertisement['user']['devices'];


        if (!$advertisement){ return $this->advertisementNotFound(); }



        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->ad_id = $advertisement->id;
        $comment->comment = $request->comment;
        $comment->save();

        $advertisement->load('comments');

        // If You want model advertisement && Comments with Children
//        $data = new Ads($advertisement);

        // If you want Model Comment Only
        $data = new Comments($comment);

        if (count($sender) > 0 && $user->id != $advertisement->user_id ) {


            $deviceUser = $sender->pluck('device');
            $this->push->sendPushNotification( $deviceUser, $deviceUser,
                'التعليقات   ', $user->name.''.' قام بالتعليق علي إعلانك '   ,  ['type'=> 21 ,'advertisement' => $advertisement->id]);

            $this->notify->NotificationDbType(21,$advertisement,$user,null,$advertisement->id);

        }

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );


    }
    public function replyComment(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $advertisement = Ad::whereId($request->advertisementId)->with('mainComments')->first();

        if (!$advertisement){ return $this->advertisementNotFound(); }

        $commentParent = Comment::whereId($request->parentComment)->where('parent_id',0)->with('children')->first();

        if (!$commentParent){  return $this->commentParentNotFound();  }

        $comment = new Comment();
        $comment->parent_id = $request->parentComment;
        $comment->user_id = $user->id;
        $comment->ad_id = $advertisement->id;
        $comment->comment = $request->comment;
        $comment->save();
// If You want model advertisement && Comments with Children
//        $data = new Ads($advertisement);

        // If you want Model Comment Only
        $data = new Comments($commentParent);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );


    }

    public function editComment(Request $request){
        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $comment = Comment::whereId($request->commentId)->first();
        if (!$comment){  return $this->commentNotFound();  }

        $commentNotForUser = Comment::whereUserId($user->id)->whereId($request->commentId)->first();

        if (!$commentNotForUser){  return $this->commentNotFoundByUser();  }

        $editComment = Comment::findOrFail($request->commentId);
        $editComment->comment = $request->comment;
        $editComment->save();

//        $data = new Ads(Ad::with('comments')->find($editComment->ad_id));

        // If you want Model Comment Only
        $data = new Comments($editComment);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }



    public function deleteComment(Request $request)
    {
        $user = User::where('api_token', $this->headerApiToken)->first();

        if (!$user) {
            return $this->UserNotFound();
        }

        $comment = Comment::whereId($request->commentId)->first();
        if (!$comment) {
            return $this->commentNotFound();
        }

        $commentNotForUser = Comment::whereUserId($user->id)->whereId($request->commentId)->first();

        if (!$commentNotForUser) {
            return $this->commentNotFoundByUser();
        }

        $comment->delete();

        if ($comment->children->count() > 0){
            $comment->children()->delete();
        }



        return response()->json( [
            'status' => 200 ,
            'message' => 'تم مسح التعليق بنجاح',
        ] , 200 );
    }




    private  function advertisementNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) 'هذ الإعلان غير متوفر'   ],200);
    }

    private  function commentParentNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) ' التعليق الرئيسي  غير متوفر'   ],200);
    }

    private  function commentNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) 'هذ التعليق غير متوفر'   ],200);
    }

    private  function commentNotFoundByUser(){
        return response()->json([   'status' => 401,  'error' => (array) 'هذ التعليق  لا ينتمي للمستخدم'   ],200);
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],401);
    }

}
