<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Http\Resources\listFavourite;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Countery;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }

    public function filter(Request $request){


        $brand =  $request->brandId  ;
        $category =  $request->catId  ;
        $color =  $request->color  ;
        $size =  $request->size  ;
        $priceFrom =  $request->price1  ;
        $priceTo =  $request->price2  ;


        $query = Product::where('is_suspend', 0)->select('id','price','category_id','images','is_offer','offer_price')->with('category');

        if ($category){  $query->where('category_id','LIKE',$category); }

        if ($brand){  $query->addSelect('brand_id')->where('brand_id','LIKE',$brand); }

        if ($size){  $query->addSelect('size')->where('size', 'like', "%\"{$size}\"%"); }

        if ($color){  $query->addSelect('colours')->where('colours', 'like', "%\"{$color}\"%"); }

        if ($priceFrom || $priceTo){  $query->whereBetween('price', [$priceFrom, $priceTo]); }


        $filter =  $query->get();


        $data = listFavourite::collection($filter);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }


    public function sort(Request $request){

        $type = $request->type;
        $brand =  $request->brandId  ;
        $category =  $request->catId  ;

        $query =  Product::where('is_suspend', 0);


        if ($category){  $query->where('category_id','LIKE',$category); }

        if ($brand){  $query->addSelect('brand_id')->where('brand_id','LIKE',$brand); }


        $filter =  $query->orderBy('price', $type ?: 'desc')->get();


        $data = listFavourite::collection($filter);


        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }



    private  function categoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا القسم غير موجود'   ],200);
    }


}
