<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\OrderItemRecource;
use App\Http\Resources\OrderRecource;
use App\Models\Cart;
use App\Models\City;
use App\Models\Order;
use App\Models\OrderItem;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

    }


    public function listOrders(Request $request){

        $user = User::whereId($request->userId)->with('orders')->first();

        if (!$user){

            return $this->UserNotFound();
        }

        $data = OrderRecource::collection($user['orders']);

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);

    }


    public function orderDetails(Request $request){

        $order = Order::whereId($request->orderId)->with('items')->first();

        if (!$order){

            return $this->orderNotFound();
        }

        $data = new OrderRecource($order);

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);

    }

    public function store(Request $request){

        $user = User::whereId($request->userId)->first();

        if (!$user){
            return $this->UserNotFound();
        }

        $city  = City::whereId($request->city)->first();

        if (!$city && $request->city){

            return $this->CityNotFound();
        }

        $cart = Cart::getCart($user);

        if (!$cart){
            return $this->CartEmpty();
        }



        $totalCart = Cart::getTotalPrice($user);
        $order = new Order();
        $order->user_id = $user->id;
        $order->another_phone =  $request->anotherPhone;
        $order->city_id =  $request->city ?: $user->city_id;
        $order->postal_code = $request->postalCode;
        $order->payment_type = $request->paymentType;
        $order->total_price = $totalCart;
        $order->save();

        foreach ($cart as $item){

            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->product_id = $item->product_id;
            $orderItem->quantity = $item->quantity;
            $orderItem->price = $item->price;
            $orderItem->size_id = $item->options['size_id'];
            $orderItem->color_id = $item->options['color_id'];
            $orderItem->save();
        }

        Cart::whereUserId($user->id)->delete();

        return response()->json([
            'status' => 200,
            'message' => "success"
        ], 200);

    }


    public function show(Request $request){

        $order = Order::find($request->order_id);

        if (!$order){
            return $this->orderNotFound();
        }

        return $order;

    }

    public function delete(Request $request){

        $order = Order::find($request->order_id);

        if (!$order){
            return $this->orderNotFound();
        }

        $order->delete();

        return response()->json([
            'status' => 200,
            'message' => "Order Delete Success"
        ], 200);

    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function CartEmpty(){
        return response()->json([   'status' => 400,  'error' => (array) trans('Empty Cart')   ],200);
    }

    private  function orderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'Order Not Found'   ],200);
    }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }


}
