<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Models\Ad;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getCategory(Request $request , Category $category) {


        $categories =  $category->newQuery();

        if ($request->has('subCategory')) {

        $subCategories  =   $category->whereParentId($request->subCategory)
                                            ->whereIsSuspend(0)->get();


        $all = \App\Http\Resources\Category::collection($subCategories);


        if(count($all) > 0){
            $data = $all->push(['id' =>(int) $request->subCategory, 'name' => 'الكل' ,'image' => $request->root().'/public/adsDefault.png' ]);

        }else{
            $data = [];
        }

            return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );
        }

        if ($request->has('adsCategory')){



            $category =  Category::whereId($request->adsCategory)->with('children')->first();

            if ($category){
                $childrenIds =  $category['children']->pluck('id');

                $ads =  Ad::where('category_id',$category->id)->orWhereIn('category_id',$childrenIds)->get();


                $data = Ads::collection($ads);

                return response()->json( [
                    'status' => 200 ,
                    'data' => $data ,
                ] , 200 );
            }else{

                return response()->json( [
                    'status' => 200 ,
                    'data' => [] ,
                ] , 200 );

            }


        }


        $mainCategires = $categories->whereParentId(0)
                                    ->whereIsSuspend(0)->get();

        $data = \App\Http\Resources\Category::collection($mainCategires);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }
}
