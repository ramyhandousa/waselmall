<?php
    
    namespace App\Http\Controllers\api;
    
    use App\Http\Controllers\Controller;
    use App\Http\Resources\Ads;
    use App\Http\Resources\following;
    use App\Http\Resources\listFavourite;
    use App\Http\Resources\notificationRecource;
    use App\Http\Resources\UserFilter;
    use App\Models\Cart;
    use App\Models\City;
    use App\Models\Notification;
    use App\Models\Ad;
    use App\Models\Countery;
    use App\Models\Image;
    use App\Models\VerifyUser;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    use UploadImage;
    use Validator;
    class UsersController extends Controller
    {
    
	 public $public_path;
	 public $defaultImage;
	 public $headerApiToken;
  
	 public function __construct( )
	 {
	        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	        app()->setLocale($language);
	    
	        $this->public_path = 'files/users/profiles';
	    
	        // default Image upload in profile user
	        $this->defaultImage = \request()->root() . '/' . 'public/assets/images/default-profile-img.png';
	    
	        // api token from header
	        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	 }

	 public function testFollower(){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

         $ads = Ads::collection(Ad::all());

         return $ads;
     }

	 public function uploadImage(Request $request){
	      
	        $image = new Image();
	        $image->url = $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage( $request , 'image' , $this->public_path );
	        $image->save();
	        return response()->json( [
		      'status' => 200 ,
		      'data' => $image ,
	        ] , 200 );
	 }

	 public function adsFollowers(){

         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

         $ids = $user->followers->pluck('id');

         if (count($ids) > 0){

             $data = Ads::collection(Ad::whereIn('user_id',$ids)->with('user')->get());

             return response()->json([
                 'status' => 200,
                 'data' => $data
             ], 200);

         }else{

             return response()->json([
                 'status' => 200,
                 'data' => []
             ], 200);

         }

     }

	 public function listFollowers(){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }


         return response()->json([
             'status' => 200,
             'data' => following::collection($user->followers)
         ], 200);
     }

	 public function followOrUnFollowUser(Request $request){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }


         if(!$user->followers()->where('follow_id', $request->followId)->first()){

            $user->followers()->attach($request->followId);
             return response()->json([
                 'status' => 200,
                 'message' => 'تم اضافته ضمن المتابعين'
             ], 200);

         }else{

             $user->followers()->detach($request->followId);

             return response()->json([
                 'status' => 201,
                 'message' => 'تم  حذفه من المتابعين'
             ], 200);
         }
     }

     public function blockUser(Request $request){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

         if(!$user->blockers()->where('blocker_id', $request->blockerId)->first()){

             $user->blockers()->attach($request->blockerId);
             return response()->json([
                 'status' => 200,
                 'message' => 'تم اضافته ضمن المحظورين'
             ], 200);

         }else{

             $user->blockers()->detach($request->blockerId);

             return response()->json([
                 'status' => 201,
                 'message' => 'تم  حذفه من المحظورين'
             ], 200);
         }
     }
	 
	 public function editProfile(Request $request){
        
         $user = User::whereId( $request->userId )->first();
         
         $valResult = Validator::make( $this->DataStudent( $request, $user ) , $this->validData( $user ) , $this->messages());

         if ($valResult->fails()) {
             return response()->json( [ 'status' => 400 , 'error' => $valResult->errors()->all() ] , 200 );
         }

         if ($request->city){

             $city  = City::whereId($request->city)->first();

             if (!$city){
                 return $this->CityNotFound();
             }
         }
         
         $user->update($this->DataStudent( $request, $user ));

         $data = new \App\Http\Resources\User($user);

         return response()->json( [
             'status' => 200 ,
             'message' => 'تم تعديل البيانات بنجاح',
             'data' => $data ,
         ] , 200 );
         
         }
         
         public function getUser(Request $request){
	        $user  = User::whereId($request->userId)
                            ->whereDoesntHave('roles')
                            ->whereDoesntHave('abilities')
                            ->first();
	     
	        if (!$user){  return $this->UserNotFound();  }

           $data = new \App\Http\Resources\User($user);
             return response()->json( [
                 'status' => 200 ,
                 'data' => $data ,
             ] , 200 );
         }

        public  function makeFavourite(Request $request) {

            $user = User::whereId($request->userId)->first();

            if ( !$user ) {  return    $this->UserNotFound();}


            $user->my_favorites()->attach($request->productId);

            return response()->json([
                'status' => 200,
                'message' => 'تم اضافته ضمن مفضلتي'
            ], 200);


        }


        public function deleteFromFavorite(Request $request){

            $user = User::whereId($request->userId)->first();

            if ( !$user ) {  return    $this->UserNotFound();}


            $user->my_favorites()->detach($request->productId);

            return response()->json([
                'status' => 200,
                'message' => 'تم  حذفه من ضمن مفضلتي'
            ], 200);
        }

        public  function  listFavourite(Request $request) {

            $user = User::whereId($request->userId)->first();

            if(!$user){  return $this->UserNotFound();  }


            $data = listFavourite::collection( $user->my_favorites);

            return response()->json([
                'status' => 200,
                'data' => $data
            ], 200);

        }


        public function listBlocking(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if(!$user){  return $this->UserNotFound();  }

            $data = UserFilter::collection( $user->blockers);

            return response()->json([
                'status' => 200,
                'data' => $data
            ], 200);
        }


        public function notification(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( ! $user ) {   return $this->UserNotFound();  }

            $notification = notificationRecource::collection( Notification::whereUserId($user->id)->get());

            return response()->json( [
                'status' => 200 ,
                'data' => $notification,
            ] , 200 );
        }


    
        private function DataStudent ( $request , $user)
        {
            $postData = [
                'first_name' => $request->first_name ?$request->first_name : $user->first_name ,
                'last_name' => $request->last_name ?$request->last_name : $user->last_name ,
                'phone' => $request->phone ?$request->phone : $user->phone ,
                'email' => $request->email ?$request->email : $user->email ,
                'password' => $request->password ,
                'image' => $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $this->defaultImage ,
                'city_id' => $request->city? $request->city : $user->city_id,
                 'address' => $request->address ?$request->address : $user->address ,
                 'data_of_birth' => $request->date_of_birth ?$request->date_of_birth : $user->data_of_birth ,
                 'gender' => $request->gender ?$request->gender : $user->gender ,
                 'zip_code' => $request->zip_code ?$request->zip_code : $user->zip_code ,
                 'company_name' => $request->company_name ?$request->company_name : $user->company_name ,
                 'company_phone' => $request->company_phone ?$request->company_phone : $user->company_phone ,
                 'company_fax' => $request->company_fax ?$request->company_fax : $user->company_fax ,
            ];

            return $postData;
        }
    
    
        private function validData ( $user )
        {
            $valRules = [
                'phone' => 'unique:users,phone,' . $user->id ,
                'email' => 'unique:users,email,' . $user->id ,
            ];
            
            return $valRules;
        }
    
        private function messages()
        {
            return [
                'email.unique' => trans('global.unique_email'),
                'phone.unique' => trans('global.unique_phone'),
            ];
        }

        private  function UserNotFound(){
            return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
        }
        private  function CityNotFound(){
            return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
        }

        private  function adNotFoundByUser(){
            return response()->json([   'status' => 401,  'error' => (array) 'هذ الإعلان  لا ينتمي للمستخدم'   ],200);
        }

    }
