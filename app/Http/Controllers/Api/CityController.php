<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\City;
use App\Models\Countery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function getCity(Request $request, Countery $countery){

        $cities =  $countery->newQuery();

        if ($request->has('subCity')) {

            $subCity  =   $countery->whereParentId($request->subCity)
                                    ->where('is_active',0)->get();

            $data = City::collection($subCity);

            return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );

        }


        $mainCategires = $cities->whereHas('children')->whereParentId(0)
                                ->where('is_active',0)->get();

        $data = City::collection($mainCategires);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }
}
