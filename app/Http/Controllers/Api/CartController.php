<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BasketResource;
use App\Http\Resources\CartResource;
use App\Http\Resources\listFavourite;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }

    public function store(Request $request){

        $user = User::whereId($request->userId)->first();

        if (!$user){  return $this->UserNotFound();  }

        $product = Product::whereId($request->productId)->first();

        if (!$product){  return $this->ProductNotFound();  }

        $additional = [
            'size_id' =>  $request->productSize  ,
            'color_id' =>  $request->productColor
        ];

         Cart::addToCart($user,$request->productId ,$request->productQuantity,$product->price, $additional);

         $response = CartItem::where('product_id',$product->id)->first();


         $data = new CartResource($response);
        return response()->json([
            'status' => 200,
            "message" => "success",
            'data' => $data
        ], 200);
    }

    public function storeMany(Request $request){

        $user = User::whereId($request->userId)->first();

        if (!$user){  return $this->UserNotFound();  }

        $favourite = $user->my_favorites;

        if ($favourite){

            $cartIds =  $favourite->pluck('id');
            $quantity =  $favourite->pluck('quantity');
            $price =  $favourite->pluck('price');

            $colours =  $favourite->pluck('colours.0');
            $size =  $favourite->pluck('size.0');

            for ($i = 0; $i < $favourite->count(); $i++){

                $userCart = Cart::firstOrCreate(['user_id' => $user->id]);

                if ($userCart->items->contains('product_id', $cartIds[$i] )) {

                    $userCartItem = CartItem::where([
                        'cart_id' => $userCart->id,
                        'product_id' => $cartIds[$i],
                    ])->first();

                    $options = $userCartItem->options;

                    $additional = [
                        'size_id' =>    $options['size_id']  ,
                        'color_id' =>   $options['color_id']  ,
                    ];

                    $userCartItem->update([
                        'quantity' => $quantity[$i] ,
                        'options'=> $additional
                    ]);

                } else {

                    $additional = [
                        'size_id' =>     $size[$i] ,
                        'color_id' =>    $colours[$i]  ,
                    ];

                    CartItem::create([
                        'cart_id' => $userCart->id,
                        'product_id' => $cartIds[$i],
                        'quantity' => $quantity[$i] ,
                        'price' => $price[$i] ,
                        'options' => $additional
                    ]);

                }
            }
        }


        return response()->json([
            'status' => 200,
            "message" => "success",
        ], 200);

    }

    public function show(Request $request){

        $user = User::whereId($request->userId)->first();

        if (!$user){  return $this->UserNotFound();  }

        $cart =  Cart::whereUserId($user->id)->with('items')->first();

        $items = BasketResource::collection($cart['items']);


        $total = Cart::getTotalPrice($user);

        $data = [ 'items' =>  $items, 'total' => $total];

        return response()->json([
            'status' => 200,
            "message" => "success",
            "data" => $data,
        ], 200);
    }

    public function edit(Request $request){

        $cartItem = CartItem::whereId($request->mainId)->first();
        if (!$cartItem){
            return $this->CartNotFound();
        }
        $cartItem->quantity = $request->quantity;
        $cartItem->save();
        return response()->json([
            'status' => 200,
            'message' => __('Site.cart.edit_to_cart_success')
        ], 200);
    }

    public function delete(Request $request){

        $cartItem = CartItem::whereId($request->id)->first();
        if (!$cartItem){
            return $this->CartNotFound();
        }
        $cartItem->delete();

        return response()->json([
            'status' => 200,
            'message' => "success Delete"
        ], 200);
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function ProductNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'Product Not found'   ],200);
    }
    private  function CartNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'Cart Not found'   ],200);
    }

}
