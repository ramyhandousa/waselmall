<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;

class changePhone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//	      'phone' => 'unique:users|numeric',
        ];
    }
       public function withValidator($validator)
       {
	    // checks user current password
	    // before making changes
	    $validator->after(function ($validator) {
	    
		 $user = DB::table('users')->where('api_token', request()->headers->get('apiToken'))->first();
		 
		$find =  DB::table('users')->where('phone','!=', $user->phone)->wherePhone( $this->newPhone)->exists();
		
		 if ( $find ) {
		     
		        $validator->errors()->add('newPhone',  trans('global.unique_phone') );
		 }
		 
	    });
	    return;
       }
       
       
       protected function failedValidation(Validator $validator) {
	    
	    $values = $validator->errors()->all();
	    
	    throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
       }
}
