<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class resgister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'first_name' => 'required|max:75',
            'last_name' => 'required|max:75',
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users|numeric',
            'password' => 'required|min:6',
            'city' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => trans('validation.required'),
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
            'phone.required' => trans('validation.required'),
            'password.required' => trans('validation.required'),
            'cityId.required' => trans('validation.required'),
        ];
    }
  

    protected function failedValidation(Validator $validator) {

//        $keys = $validator->errors()->keys();
        $values = $validator->errors()->all();

//        $rules = array_combine($keys,$values);


        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }


}
