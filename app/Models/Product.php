<?php

namespace App\Models;

use App\Image;
use App\myImage;
use App\Provider;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Product extends Model
{

    use Translatable;
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['name', 'description' ,'price','quantity','images'];
 
    protected $casts = [ 'images' => 'array' ,'colours' => 'array' , 'size' => 'array'];


    protected $hidden = [
         'translations'
    ];

       public function user() {
	        return $this->belongsTo(User::class);
       }

       public function orders()
       {
            return $this->hasMany(Order::class);
       }


       public function product_purchases()
       {
            return $this->hasMany(ProductPurchase::class);
       }

       public function category()
       {
            return $this->belongsTo(Category::class);
       }


       public function brand()
       {
            return $this->belongsTo(Brand::class);
       }

       public function currency()
       {
            return $this->belongsTo(Currency::class);
       }

       public function brand_filter()
       {
            return $this->belongsTo(Brand::class,'brand_id')
                ->where('is_suspend',0);
       }



       public function offer_day()
       {
            return $this->hasOne(DealOfDay::class,'product_id')
                        ->whereDate('end_data', '>=' ,Carbon::now());
       }


       

}
