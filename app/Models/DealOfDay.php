<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealOfDay extends Model
{

    protected $fillable = ['product_id','end_data'];

    protected $hidden = [
        'created_at','updated_at'
    ];
    public function product(){
        return $this->belongsTo(Product::class)->with('currency');
    }


}
