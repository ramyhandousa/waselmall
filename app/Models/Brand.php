<?php

namespace App\Models;

use App\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    use Translatable;

    public $translatedAttributes = ['name' ];

    protected $fillable = ['name','image'];

    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];

    public function users() {
        return $this->hasMany(User::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

}
