<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{

    use Translatable;


    public $translatedAttributes = ['name' ];
    protected $hidden = [
        'created_at', 'updated_at', 'translations'
    ];

}
