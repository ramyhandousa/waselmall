<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPurchase extends Model
{

    protected $hidden = [
        'created_at','updated_at'
    ];

    public function purchase(){
        return $this->belongsTo(PurchaseType::class,'purchase_type_id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    public function product_name(){
        return $this->belongsTo(Product::class,'product_id')->select('id');
    }



}
