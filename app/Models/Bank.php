<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Bank extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];
    protected $fillable = ['name','account_number', 'is_published'];
       
       protected $hidden = [
	     'created_at',  'updated_at',
       ];
}
