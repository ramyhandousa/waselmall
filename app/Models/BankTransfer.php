<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{

    
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    
    public function mybank(){
        return $this->belongsTo(Bank::class,'bank');
    }

}
