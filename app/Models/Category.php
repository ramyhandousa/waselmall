<?php

namespace App\Models;

use App\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Translatable;
    public $translatedAttributes = ['name' ];
    protected $fillable = ['name','image','parent_id','is_suspend'];
    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];
     
       public function users() {
	    return $this->belongsToMany(User::class);
       }
       
       public function children()
       {
	    return $this->hasMany(Category::class, 'parent_id');
       }
       
       
       public function parent(){
	    return $this->belongsTo(Category::class,'parent_id');
       }
       
       public function parent_filter(){
	    return $this->belongsTo(Category::class,'parent_id')->select('id','parent_id');
       }
       
       
       public function product(){
	    return $this->hasMany(Product::class,'category_id');
       }
       public function scopeIsActive($query)
       {
	    return $query->whereIsSuspend(0);
       }
}
