<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = ['cart_id', 'product_id' ,'quantity','price','options'];

    protected $casts = [ 'options' => 'array' ];

    protected $hidden = [
        'created_at','updated_at'
    ];


//    public function getOptionAttribute()
//    {
//        if (isset($this->attributes['option'])) {
//            return 'dasd';
//            if ($this->attributes['option']) {
//
//                return  '1112';
//            }
//        }
//
//    }

//    public function getOptionAttribute (){
//      return  (int) $this->attributes['option->size_id'];
//    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }


    public function product_filter(){
        return $this->belongsTo(Product::class,'product_id')->select('id','category_id','images')->with('category');
    }


}
