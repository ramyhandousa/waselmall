<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{


    protected $fillable = ['user_id' ];



    public function items()
    {
        return $this->hasMany(CartItem::class);
    }


    public function items_filter()
    {
        return $this->hasMany(CartItem::class)->with('product_filter');
    }


    public static function addToCart($user ,$productId,$quantity ,$price , $additional =null)
    {
        $cart = [];

            $userCart = Cart::firstOrCreate(['user_id' => $user->id]);

            if ($userCart->items->contains('product_id', $productId )) {

                $userCartItem = CartItem::where([
                    'cart_id' => $userCart->id,
                    'product_id' => $productId,
                ])->first();

                $options = $userCartItem->options;

//                $additional = [
//                    'size_id' =>  request()->size  ? : $options['size_id']  ,
//                    'color_id' =>  request()->color  ?: $options['color_id']  ,
//                ];
                $additional = [
                    'size_id' =>   request('productSize') ? : $options['size_id']  ,
                    'color_id' =>   request('productColor') ?: $options['color_id']  ,
                ];

                $userCartItem->update([
                    'quantity' => $quantity   ,
                    'options'=> $additional
                ]);


            } else {

                  CartItem::create([
                    'cart_id' => $userCart->id,
                    'product_id' => $productId,
                    'quantity' => $quantity ,
                    'price' => $price ,
                    'options' => $additional
                ]);

            }

        return $cart;
    }


    public static function getCart($user ){



            $cart = Cart::whereUserId($user->id)->first();

            if ($cart){


                $cartItems = [];

                foreach ($cart->items_filter as $value){
                    $cartItems[] =  $value  ;
                }

                return $cartItems;
            }





    }

    public static function getIdCart($user ){



            $cart = Cart::whereUserId($user->id)->first();

            if ($cart){

                return $cart->id;
            }





    }

    public static function getTotalPrice($user ){

        $cart = Cart::whereUserId($user->id)->first();


        if ($cart){

            return $cart->items->pluck('price')->sum();

        }else{

            return 0;

        }

    }




}
