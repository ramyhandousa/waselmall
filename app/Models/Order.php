<?php

namespace App\Models;

use App\Provider;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
       protected $fillable =
	     [
		 'user_id',
		 'city_id',
		 'another_phone',
		 'postal_code',
		 'payment_type',
		 'status',
		 'total_price',
		 'message'
      	     ];
       
       public function user() {
              return $this->belongsTo(User::class);
       }



    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

       public function city() {

	        return $this->belongsTo(City::class);
       }


    public static function getTotalItems($id ){

        $order = Order::whereId($id)->first();


        if ($order){

            return $order->items->pluck('order_id')->sum();

        }else{

            return 0;

        }

    }


//    public function getStatusAttribute()
//    {
//        $status = $this->attributes['status'];
//        switch ($status) {
//            case '0':
//                return 'الطلب معلق';
//                break;
//            case '1':
//                return 'جاري تجهيز الطلب';
//                break;
//            case '-1':
//                return 'ملغي';
//                break;
//            case '2':
//                return 'تم الإنتهاء';
//                break;
//            default:
//                return '';
//        }
//    }
       
       
       
       
}
