<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PurchaseType extends Model
{

    use Translatable;
    public $translatedAttributes = ['name'];
    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];
    public function products(){

        return $this->hasMany(ProductPurchase::class);
    }


}
