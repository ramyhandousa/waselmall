<?php

namespace App;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
       
       
       public function product()
       {
	    return $this->belongsTo(Product::class);
       }
}
