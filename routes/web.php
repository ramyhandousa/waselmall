<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();



Route::get('/', 'HomeController@index')->name('home')->middleware('validWebSite');


Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');
    
    Route::group(['prefix' => 'administrator'], function () {


        Route::get('/login', 'Admin\LoginController@login')->name('admin.login');
        Route::post('/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');
        
        // Password Reset Routes...
        
        Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
        Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
        Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
        Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');
        
    });
    
    Route::group(['prefix' => 'administrator', 'middleware' => ['admin']], function () {
        
        Route::get('/', 'Admin\HomeController@index')->name('home');
        Route::get('/home', 'Admin\HomeController@index')->name('admin.home');
    
    
        // administrators --- Admin Panel users
        // add admin or some one who controll Or help for admin panel
    
        Route::resource('helpAdmin', 'Admin\HelpAdminController');
        Route::get('helpAdmin/{id}/delete', 'Admin\HelpAdminController@delete')->name('helpAdmin.delete');
        Route::post('helpAdmin/{id}/delete', 'Admin\HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
        Route::post('helpAdmin/{id}/suspend', 'Admin\HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
        Route::get('user/{id}/delete', 'Admin\UsersController@delete')->name('user.for.delete');
        Route::post('user/suspend', 'Admin\HelpAdminController@suspend')->name('user.suspend');
        // Roles routes ..........
        Route::resource('roles', 'Admin\RolesController');
        Route::post('role/delete', 'Admin\RolesController@delete')->name('role.delete');
    
    
        Route::resource('users', 'Admin\UsersController');
        Route::post('accepted','Admin\UsersController@accpetedUser')->name('user.accepted');
    
        Route::get('settings/aboutus', 'Admin\SettingsController@aboutus')->name('settings.aboutus');
        Route::get('settings/taxs', 'Admin\SettingsController@taxs')->name('settings.taxs');
        Route::get('settings/terms', 'Admin\SettingsController@terms')->name('settings.terms');
        Route::get('settings/suspendElement', 'Admin\SettingsController@suspendElement')->name('settings.suspendElement');

//        Route::resource('faqs', 'Admin\FaqsController');

        Route::get('/settings/app-general-settings', 'Admin\SettingsController@appGeneralSettings')->name('settings.app.general');
        Route::get('settings/contacts', 'Admin\SettingsController@contactus')->name('settings.contactus');


        Route::post('/settings', 'Admin\SettingsController@store')->name('administrator.settings.store');
    
        Route::post('contactus/reply/{id}', 'Admin\SupportsController@reply')->name('support.reply');
        Route::get('contactus', 'Admin\SupportsController@index')->name('support.index');
        Route::get('contactus/{id}', 'Admin\SupportsController@show')->name('support.show');
        Route::post('support/contact/delete', 'Admin\SupportsController@delete')->name('support.contact.delete');


        Route::resource('supports', 'Admin\SupportsController');
        Route::post('supports/delete', 'Admin\SupportsController@delete')->name('supports.delete');

        Route::resource('types', 'Admin\TypesSupportController');


//        Route::get('public/notifications', 'Admin\NotificationsController@publicNotifications')->name('public.notifications');
//        Route::get('public/notifications/create', 'Admin\NotificationsController@createPublicNotifications')->name('create.public.notifications');


        Route::post('city/delete/group', 'Admin\CitiesController@groupDelete')->name('cities.group.delete');
        Route::post('cities/delete', 'Admin\CitiesController@delete')->name('city.delete');
        Route::resource('cities', 'Admin\CitiesController');
        Route::post('city/suspend', 'Admin\CitiesController@suspend')->name('city.suspend');

        // -------------------------------------- categories .................
        Route::resource('categories', 'Admin\CategoriesController');
        Route::post('categories/delete', 'Admin\CategoriesController@delete')->name('categories.delete');
        Route::post('categories/suspend', 'Admin\CategoriesController@suspend')->name('categories.suspend');

        // -------------------------------------- brands .................
        Route::resource('brands', 'Admin\BrandController');
        Route::post('brands/delete', 'Admin\BrandController@delete')->name('brands.delete');
        Route::post('brands/suspend', 'Admin\BrandController@suspend')->name('brands.suspend');

        // -------------------------------------- sizes .................
        Route::resource('sizes', 'Admin\SizeController');
        Route::post('sizes/delete', 'Admin\SizeController@delete')->name('sizes.delete');


        // -------------------------------------- currencies .................
        Route::resource('currencies', 'Admin\CurrencyController');
        Route::post('currencies/delete', 'Admin\CurrencyController@delete')->name('currencies.delete');


        // -------------------------------------- colors .................
        Route::resource('colors', 'Admin\ColorController');
        Route::post('colors/delete', 'Admin\ColorController@delete')->name('colors.delete');



        // -------------------------------------- purchaseTypes .................
        Route::resource('purchaseTypes', 'Admin\purchaseTypesController');
        Route::post('purchaseTypes/delete', 'Admin\purchaseTypesController@delete')->name('purchaseTypes.delete');

        // -------------------------------------- purchaseTypes .................
        Route::resource('dealDays', 'Admin\DealOfDaysController');
        Route::post('dealDays/delete', 'Admin\DealOfDaysController@delete')->name('dealDays.delete');



        // -------------------------------------- products .................
        Route::resource('products', 'Admin\ProductsController');
        Route::post('products/delete', 'Admin\ProductsController@delete')->name('products.delete');
        Route::post('products/suspend', 'Admin\ProductsController@suspend')->name('products.suspend');


        Route::resource('reports', 'Admin\ReportsController');
        
        Route::post('/logout', 'Admin\LoginController@logout')->name('administrator.logout');
        
    });



Route::get('/sub', function (Illuminate\Http\Request $request) {

    $cities =\App\Models\City::whereParentId($request->id)->get();

    if (!empty($cities) && count($cities) > 0){
        return response()->json( $cities);
    }else{

        return response()->json(401);
    }


})->name('getSub');



Route::group([
    'namespace' => 'Site',
    'middleware' => 'localeControlPanel',
], function () {

    Route::group(['prefix' => 'Auth','namespace' => 'Auth'], function () {

        Route::get('/registerSite','RegisterController@index')->name('site.get.register');
        Route::get('/loginSite','LoginController@index')->name('site.get.login');

        Route::get('/resetEmail','ResetController@resetEmail')->name('site.resetEmail');
        Route::get('/updatePassword','ResetController@updatePassword')->name('site.updatePassword');

        Route::post('/registerAccount','RegisterController@register')->name('site.registerSite');
        Route::post('/LoginSite','LoginController@login')->name('site.login');

        Route::post('/resetEmail','ResetController@sendMail')->name('site.sendMail');
        Route::post('/updatePassword','ResetController@updatePass')->name('site.updatePass');

        Route::post('/logOutSite','LoginController@logOut')->name('site.logOut');


    });

    Route::group(['prefix' => 'products' ], function () {

        Route::get('/','ProductsController@index')->name('site.getAllProducts');
        Route::get('/show','ProductsController@show')->name('site.product.show');

    });

    Route::group(['prefix' => 'searchSite' ], function () {

        Route::get('/','SearchController@search')->name('site.search');

    });

    Route::group(['middleware' => 'validWebSite'], function () {


        Route::resource('usersSite', 'UsersController');

        Route::get('/editAddress','UsersController@editAddress')->name('site.editAddress');
        Route::get('/listFavourite','UsersController@listFavourite')->name('site.listFavourite');
        Route::post('/addAddress','UsersController@addAddress')->name('site.user.addAddress');


        Route::resource('cartSite', 'CartController');
        Route::post('/cart/delete','CartController@delete')->name('site.cart.delete');

        Route::group(['prefix' => 'ordersSite' ], function () {

            Route::get('/','OrderController@index')->name('site.getAllOrders');
            Route::post('/storeOrder','OrderController@storeOrder')->name('site.storeOrder');

        });

    });


    Route::post('/makeFavourite','UsersController@makeFavourite')->name('site.user.makeFavourite');

    Route::group(['prefix' => 'setting'], function () {

        Route::get('/SiteAbout','SettingController@aboutUs')->name('site.aboutUs');
        Route::get('/SitePrivacy','SettingController@Privacy')->name('site.privacy');
        Route::get('/SiteContact','SettingController@contactUsIndex')->name('site.contactUs');
        Route::post('/SiteContact','SettingController@contactUs')->name('site.contactUs');

    });

    Route::get('/','indexController@index')->name('firstPageSite');


});



