<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group([
    'namespace' => 'Api',
], function () {

        Route::post('signin', 'LoginController@login');
        Route::post('signup', 'RegisterController@register');
        Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('editPassword', 'ResetPasswordController@changPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resetCode', 'ResetPasswordController@resendCode');
        Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');
        Route::post('editProfile', 'UsersController@editProfile');
        Route::post('logOut','LoginController@logOut');



        Route::post('profile','UsersController@getUser');
        Route::post('userFavorite','UsersController@listFavourite');
        Route::post('addToFavorite','UsersController@makeFavourite');
        Route::post('deleteFromFavorite','UsersController@deleteFromFavorite');




         Route::post('add-to-basket','CartController@store');
         Route::post('add-many-to-basket','CartController@storeMany');
         Route::post('show-basket','CartController@show');
         Route::post('edit-basket','CartController@edit');
         Route::post('delete-basket','CartController@delete');


         Route::post('product','ProductsController@listProducts');
         Route::post('category','ProductsController@listProductsOfCategory');
         Route::post('offers','ProductsController@offers');
         Route::post('brand','ProductsController@brands');

         Route::post('make-order','OrderController@store');
         Route::post('unpaiedOrders','OrderController@listOrders');
         Route::post('orderDetails','OrderController@orderDetails');
         Route::post('delete-order','OrderController@delete');



         Route::post('filter','SearchController@filter');
         Route::post('sort','SearchController@sort');




        Route::post('aboutUs','SettingController@aboutUs');
        Route::post('policy','SettingController@terms');
        Route::post('contactUs','SettingController@contactUs');
        Route::post('bankTransfer','SettingController@bankTransfer');



});





//    Route::post('testResource','api\TestController@testResource');
//    Route::get('testcategory','api\TestController@testcategory');
//    Route::get('getAllAds','api\TestController@getAllAds');

//    Route::get('searchAds','api\SearchController@searchAds');
    Route::get('ListComment','api\CommentsController@ListComment');


    Route::post('testNotify','api\TestController@testNotify');




