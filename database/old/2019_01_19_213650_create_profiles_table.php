<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
    
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
    
            $table->string('national_card_image')->nullable();
            $table->integer('national_card_number')->nullable();
            $table->string('license_owner')->nullable();
            $table->string('license_driver')->nullable();
            $table->string('truck_type')->nullable();
            $table->string('truck_model')->nullable();
            $table->string('truck_number')->unique()->nullable();
            $table->string('app_percentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
