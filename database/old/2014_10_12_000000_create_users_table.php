<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone')->unique();
            $table->tinyInteger('parent_id')->default(0);
            $table->tinyInteger('is_user')->default(0);
            $table->string('email')->unique();
            $table->string('api_token')->unique();
            $table->string('password');
            $table->text('address')->nullable();
            $table->string('latitute')->nullable();
            $table->string('longitute')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('is_accepted')->default(0);
            $table->tinyInteger('is_suspend')->default(0);
            $table->text('message')->nullable();
            $table->integer('login_count')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
